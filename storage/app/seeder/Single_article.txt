                                                               FACTURE - 1

                                                     Date de   facturation: 01/03/2021

                                                               Échéance: 31/03/2021

Description                        Date  Qté   Prix  unitaire  TVA                      Montant

T-Shirt                  01/03/2021      1,00        10,00 €   20,0 %                   12,00 €

                                                               Total HT                 10,00 €

                                                               TVA 20,0 %               2,00 €

                                                               Total (TTC)              12,00 €

Conditions de paiement:  30 jours

Échéance:                31/03/2021
