                                                                      FACTURE - 001

                                                                      Date de facturation: 01/03/2021

                                                                                Échéance: 31/03/2021

URIEN Mickaël

4 Allée des belles-rues

Paris 75000

Description                          Date     Qté               Prix  unitaire  TVA                    Montant

TV                       01/03/2021           1,00                    500,00 €  20,0 %                 600,00 €

Télécommande             01/03/2021           1,00                    15,00 €   20,0 %                 18,00 €

Fauteil                  01/03/2021           2,00                    50,00 €   20,0 %                 120,00 €

                                                                                Total HT               615,00 €

                                                                                TVA 20,0 %             123,00 €

                                                                                Total (TTC)            738,00 €

Moyens de paiement:      Banque: BNP Paribas

                         SWIFT/BIC: BNP12345

                         IBAN: FR0917569000409431375233G42

Conditions de paiement:  30 jours

Échéance:                31/03/2021

                                           Note de bas de page
