                                                 Facture N° 879161                                                   Vous avez été reçu par : Adrien C.

                                                                                                                     Véhicule restitué par : Damien P.

                                                           Etabli le 29/04/2021

FEU VERT QUIMPER 2 GEANT           Code APE : 4532                            Feu Vert SAS                           N° TVA intra : FR57327359980

165 route de Benodet               SIRET : 32735998001005                     11, allée du moulin Berger             Forme juridique : ETS

Parking Geant                      N° agrément climatisation : 3103120        B.P. 70162                             Capital : 25 000 000.00 euros

29196 QUIMPER                                                                 69136 ECULLY                           RCS : 327359980

Téléphone : 02 98 10 24 40                                                    Téléphone : 04 26 10 58 00             SIREN : 327359980

Fax : 02 98 10 13 44                                                          Fax : 04 26 10 59 01

                  Immatriculation : 9934YY29                                                             DONVAL WILLIAM

PEUGEOT 406                        Type mine : MPE5202AV451                   85 CHEMIN DU VUZUT

                                   Numéro de série :                          29900 CONCARNEAU                       Numéro client : 19522418

Mise en circulation : 01/08/1999   VF38BRHYE80861581                          Téléphone : 0781326901

Kilométrage : 272540               Motorisation : 2.0 HDI 90 1997cm3 90cv     Email :                                                  Numéro de carte

Prochain contrôle technique :      (66) R                                                                                              2100053067635

                                   Genre : VP                                 WILLIAM.DONVAL@GMAIL.COM

03/06/2022                         Carburant : Diesel                         Fixe :

      Produit                      Désignation             Marque          Qté    Tarif TTC    Remise %      P.U.HT  Prix net TTC  Total TTC   Technicien      Code

                                                                                                                                                               TVA

483035            PLAQUETTES FREIN AVANT + DISQUES + POSE  PREMIUM         1.00        204.90     0.00       170.75  204.90            204.90  Baptiste A      8

SIM-FR-V2

482492            *   MO FORF PLAQ+DISQ AV POSEES T2       FEU VERT           1                                                                Baptiste A

743764                * DIAGNOSTIC SÉCURITÉ GRATUIT        FEU VERT           1                                                                Baptiste A

DIAGG

0238800705440001                   * DISQUES               Divers             1

DF841

0238800705440002                   * PLAQUETTES            Divers             1

DB1063

001309                VIDANGE LIQUIDE FREIN + PURGE        FEU VERT        1.00        63.90                 53.25   63.90             63.90   Baptiste A      8

      VCF

064993                NETT DEGRAISS 600ML BARDAHL          BARDAHL         1.00        8.99                  7.49    8.99              8.99                    8

4455F

Sous-total        Freinage :                                                                                                                          277.79

431297            CONTRIBUTION RECYCLAGE DECHETS           Divers          1.00        2.54                  2.12    2.54              2.54    Baptiste A      8

Commentaire à l'attention du client : Feu Vert veille au recyclage de tous les déchets liés à son activité auprès de

filières accréditées

486771                PARTICIPATION PROTECTIONS COVID-19   Divers          1.00        3.90                  3.25    3.90              3.90    Baptiste A      8

Commentaire à l'attention du client : La période actuelle implique des procédures exceptionnelles dans la prise en                                                   Données techniques : *238*V*ATE*228176*6*VTE N°879161*OID N°8616885* UEDI 0004817 29/04/2021 H:17.10

charge de nos clients et de leur véhicule. Une préparation sanitaire est réalisée conformément aux gestes barrière qui

nous  sont     imposés.        Le  montant    de  cette    prise  en  charge  est      répercuté  à      sa  valeur  réelle        et  sans    marge.      Il  est

évidemment susceptible d'évoluer en fonction de nos coûts d'achat et des opérations spécifiques que nous serons tenus

de réaliser.

Sous-total Environnement :                                                                                                                                 6.44

                                                                      Page : 1/2

                      Retrouvez tous vos devis, factures atelier et compte fidélité sur votre espace client en ligne (www.feuvert.fr)
                                                                           Facture N° 879161                                           Vous avez été reçu par : Adrien C.

                                                                                                                                       Véhicule restitué par : Damien P.

                                                                                       Etabli le 29/04/2021

FEU VERT QUIMPER 2 GEANT          Code APE : 4532                                                          Feu Vert SAS                        N° TVA intra : FR57327359980

165 route de Benodet              SIRET : 32735998001005                                                   11, allée du moulin Berger          Forme juridique : ETS

Parking Geant                     N° agrément climatisation : 3103120                                      B.P. 70162                          Capital : 25 000 000.00 euros

29196 QUIMPER                                                                                              69136 ECULLY                        RCS : 327359980

Téléphone : 02 98 10 24 40                                                                                 Téléphone : 04 26 10 58 00          SIREN : 327359980

Fax : 02 98 10 13 44                                                                                       Fax : 04 26 10 59 01

                      Immatriculation : 9934YY29                                                                                   DONVAL WILLIAM

PEUGEOT 406                                                        Type mine : MPE5202AV451                85 CHEMIN DU VUZUT

                                                                   Numéro de série :                       29900 CONCARNEAU                Numéro client : 19522418

Mise en circulation : 01/08/1999                                   VF38BRHYE80861581                       Téléphone : 0781326901

Kilométrage : 272540                                               Motorisation : 2.0 HDI 90 1997cm3 90cv  Email :                             Numéro de carte

Prochain contrôle technique :                                      (66) R                                                                          2100053067635

                                                                   Genre : VP                              WILLIAM.DONVAL@GMAIL.COM

03/06/2022                                                         Carburant : Diesel                      Fixe :

25020879161000000012              24060443232002842322

                                                                                                           Code TVA      Taux TVA      HT          TVA            TTC

                                                                                                                    8    20.00         236.86      47.37              284.23

                                                                                                           Cumul                       236.86      47.37          284.23

                                                                                                           Net à payer                                            284.23

                                                                                                           Acompte                                                    74.82

                                                                                                           Restant dû                                             209.41

                                                                                             Clause de réserve de propriété au verso ; Frais de recouvrement pour tout retard de paiement : 40 euros

En cas de litige, et après réclamation écrite faite auprès du Vendeur demeurée infructueuse, le client peut saisir gratuitement le Médiateur du Conseil National des Professions de l'Automobile soit 50, rue Rouget de Lisle, 92158 SURESNES

CEDEX (www.mediateur-cnpa.fr) soit contact@mediateur-cnpa.fr ou en cas d’achat en ligne le Service du Médiateur du e-commerce de la FEVAD : 60, rue de la Boétie, 75008 PARIS www.mediateurfevad.fr (ou tout autre médiateur agréé

dont les coordonnées sont affichées dans votre centre franchisé).

Pour cet achat de 284.23EUR : 2 mensualités de 94.74EUR et une dernière mensualité de 94.75EUR . Montant total dû : 284.23EUR

TAEG fixe : 0% . Durée : 3 mois.

Un crédit vous engage et doit être remboursé. Vérifiez vos capacités de remboursement avant de vous engager.

* Crédit amortissable proposé de 100EUR à 4000EUR de 3x à 20x consenti sous réserve d'acceptation par COFIDIS SA à Directoire et Conseil de

surveillance au capital de  67 500 000EUR. Conditions valables au 01/01/2018. Cette simulation s'entend pour un financement le 29/04/2021 et une                                                                                               Données techniques : *238*V*ATE*228176*6*VTE N°879161*OID N°8616885* UEDI 0004817 29/04/2021 H:17.10

première échéance le 05/06/2021. Siège social : Parc de la Haute Borne, 61 avenue Halley 59866 VILLENEUVE D'ASCQ Cedex - RCS LILLE

METROPOLE SIREN N 325 307 106 enregistré auprès de l'ORIAS sous le numéro 07 02 34 93. Vous bénéficiez d'un délai légal de rétractation de 14

jours.. Feu Vert est mandaté à titre non exclusif afin d'apporter son concours à la réalisation du contrat de crédit sans agir en qualité de prêteur, il ne

dispose d'aucun pouvoir d'octroi ou de gestion du crédit. N ORIAS 13 005 987. Pour les paiements en plusieurs fois sans frais : taux débiteur fixe 0%.

Pour les paiements en plusieurs fois avec frais : taux débiteur fixe 13,88%. Coût du crédit pris en charge par Cofidis pour les paiements en plusieurs

fois sans frais.

                                                                                             Page : 2/2

                      Retrouvez tous vos devis, factures atelier et compte fidélité sur votre espace client en ligne (www.feuvert.fr)
