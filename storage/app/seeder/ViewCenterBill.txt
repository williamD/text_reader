                                                                           Facture N° 879116                                                       Vous avez été reçu par : Agnes B.

                                                                                                                                                   Véhicule restitué par : Adrien C.

                                                                                       Etabli le 24/04/2021

FEU VERT QUIMPER 2 GEANT           Code APE : 4532                                                         Feu Vert SAS                                   N° TVA intra : FR57327359980

165 route de Benodet               SIRET : 32735998001005                                                  11, allée du moulin Berger                     Forme juridique : ETS

Parking Geant                      N° agrément climatisation : 3103120                                     B.P. 70162                                     Capital : 25 000 000.00 euros

29196 QUIMPER                                                                                              69136 ECULLY                                   RCS : 327359980

Téléphone : 02 98 10 24 40                                                                                 Téléphone : 04 26 10 58 00                     SIREN : 327359980

Fax : 02 98 10 13 44                                                                                       Fax : 04 26 10 59 01

               Immatriculation : 9934YY29                                                                                              DONVAL WILLIAM

PEUGEOT 406                                                        Type mine : MPE5202AV451                85 CHEMIN DU VUZUT

                                                                   Numéro de série :                       29900 CONCARNEAU                            Numéro client : 19522418

Mise en circulation : 01/08/1999                                   VF38BRHYE80861581                       Téléphone : 0781326901

Kilométrage : 272356                                               Motorisation : 2.0 HDI 90 1997cm3 90cv  Email :                                                Numéro de carte

Prochain contrôle technique :                                      (66) R                                                                                         2100053067635

                                                                   Genre : VP                              WILLIAM.DONVAL@GMAIL.COM

03/06/2022                                                         Carburant : Diesel                      Fixe :

      Produit                      Désignation                                         Marque              Qté   Tarif TTC   Remise %      P.U.HT  Prix net TTC Total TTC  Technicien                                                              Code

                                                                                                                                                                                                                                                   TVA

027157                      DIAGNOSTIC FREINAGE                                        FEU VERT            1.00     19.00      0.00        15.83       19.00      19.00    Adrien S.                                                               8

DIAG

Commentaire à l'attention du client : voir bruit au freinage

Sous-total Freinage :                                                                                                                                                                                                                          19.00

431297         CONTRIBUTION RECYCLAGE DECHETS                                          Divers              1.00        2.54                2.12        2.54       2.54     Adrien S.                                                               8

Commentaire à l'attention du client : Feu Vert veille au recyclage de tous les déchets liés à son activité auprès de

filières accréditées

486771                PARTICIPATION PROTECTIONS COVID-19                               Divers              1.00        3.90                3.25        3.90       3.90     Adrien S.                                                               8

Commentaire à l'attention du client : La période actuelle implique des procédures exceptionnelles dans la prise en

charge de nos clients et de leur véhicule. Une préparation sanitaire est réalisée conformément aux gestes barrière qui

nous  sont     imposés.        Le  montant                         de      cette       prise  en  charge   est      répercuté  à       sa  valeur  réelle     et  sans     marge.                                                              Il  est

évidemment susceptible d'évoluer en fonction de nos coûts d'achat et des opérations spécifiques que nous serons tenus

de réaliser.

Sous-total Environnement :                                                                                                                                                                                                                     6.44

                                   25060879116000254411

                                                                                                           Code TVA          Taux TVA              HT             TVA        TTC                                                                         Données techniques : *238*V*ATE*227934*6*VTE N°879116*OID N°* UEDI 0004471 24/04/2021 H:17.28

                                                                                                                    8        20.00                 21.20          4.24           25.44

                                                                                                           Cumul                                   21.20          4.24           25.44

                                                                                                           Net à payer                                                           25.44

                                                                                               Clause de réserve de propriété au verso ; Frais de recouvrement pour tout retard de paiement : 40 euros

En cas de litige, et après réclamation écrite faite auprès du Vendeur demeurée infructueuse, le client peut saisir gratuitement le Médiateur du Conseil National des Professions de l'Automobile soit 50, rue Rouget de Lisle, 92158 SURESNES

CEDEX (www.mediateur-cnpa.fr) soit contact@mediateur-cnpa.fr ou en cas d’achat en ligne le Service du Médiateur du e-commerce de la FEVAD : 60, rue de la Boétie, 75008 PARIS www.mediateurfevad.fr (ou tout autre médiateur agréé

dont les coordonnées sont affichées dans votre centre franchisé).

                                                                                                  Page : 1/1

                      Retrouvez tous vos devis, factures atelier et compte fidélité sur votre espace client en ligne (www.feuvert.fr)
