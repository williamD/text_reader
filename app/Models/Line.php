<?php

namespace App\Models;

use App\Models\Invoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Line extends Model
{
    use HasFactory;

    protected $fillable = ['invoice_id', 'reference', 'data', 'object', 'quantity', 'price_ht', 'price_ttc', 'amount', 'tax', 'tax_amount', 'other'];

    public function invoice() {
        return $this->belongsTo(Invoice::class);
    }
}
