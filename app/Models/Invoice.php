<?php

namespace App\Models;

use App\Models\Line;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Invoice extends Model
{
    use HasFactory;
    
    protected $fillable = ['path'];

    public function lines() {
        return $this->hasMany(Line::class);
    }
}
