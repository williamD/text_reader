<?php

namespace App\Http\Helpers;

use App\Http\Helpers\Data;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

function sortByLongestString(string $a, string $b): int {
    return mb_strlen($b) - mb_strlen($a);
}

function sortByShortestString(string $a, string $b): int {
    return mb_strlen($a) - mb_strlen($b);
}

function sortByLongestPosition($a, $b) {
    return $b['position']['start'] - $a['position']['start'];
}

function sortByShortestPosition($a, $b) {
    return $a['position']['start'] - $b['position']['start'];
}

class Utils {
    
    /**
     * Split string by taking care of the utf-8 encoding
     * 
     * @param string $string
     * @param int $length
     * 
     * @return array
     */
    public static function str_split_unicode(string $string, int $length = 0): array {
        if ($length > 0) {
            $result = array();
            $str_length = mb_strlen($string, "UTF-8");
            for ($i = 0; $i < $str_length; $i += $length) {
                $result[] = mb_substr($string, $i, $length, "UTF-8");
            }
            return $result;
        }
        return preg_split("//u", $string, -1, PREG_SPLIT_NO_EMPTY);
    }
    
    /**
     * Split a string by spaces
     * 
     * @param int $space_number
     * @param string $string
     * 
     * @return array
     */
    public static function splitBySpace(int $space_number, string $string): array {
        return preg_split('/ {'.$space_number.',}/', $string);
    }
    
    /**
     * Split a string by lines (end of line character)
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function splitByLine(string $string): array {
        return explode("\n", $string);
    }

    /**
     * Custom floatval function, replace commas by dots
     * 
     * @param mixed $val
     * 
     * @return float
     */
    public static function floatvalue($val){
        $val = str_replace(",",".",$val);
        $val = preg_replace('/\.(?=.*\.)/', '', $val);
        return floatval($val);
    }
    
    /**
     * Check if a string contains at least one word from array
     * 
     * @param string $string
     * @param array $array
     * 
     * @return bool
     */
    public static function containsWord(string $string, array $array): bool {
        foreach($array as $value) {
            if(preg_match('/\b'.$value.'\b/', $string)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if string contains one word from array sorted by longest string
     * 
     * @param string $string
     * @param array $array
     * 
     * @return mixed
     */
    public static function containsLongest(string $string, array $array) {
        usort($array, 'sortByLongestString');
        foreach($array as $value) {
            if(Str::contains(Str::lower($string), $value)) {
                return $value;
            }
        }
        return false;
    }

    /**
     * Check if string contains one word from array sorted by shortest string
     * 
     * @param string $string
     * @param array $array
     * 
     * @return mixed
     */
    public static function containsShortest(string $string, array $array) {
        usort($array, 'sortByShortestString');
        foreach($array as $value) {
            if(Str::contains(Str::lower($string), $value)) {
                return $value;
            }
        }
        return false;
    }

    /**
     * Get the closest number contained in array from the given number
     * 
     * @param int|float $search
     * @param int[]|float[] $array
     * @return null|int|float
     */
    function getClosest($search, array $array) {
        $closest = null;
        $arrayCopy = $array;
        sort($arrayCopy);
        foreach($arrayCopy as $item) {
           if($closest === null || abs($search - $closest) > abs($item - $search)) {
              $closest = $item;
           }
        }
        return $closest;
    }

    /**
     * Custom preformatted var_dump function
     * 
     * @param mixed $variable
     */
    public static function dump($variable) {
        echo '<pre>';
        var_dump($variable);
        echo '</pre>';
    }

    /**
     * Get elements contained in a string
     * 
     * @param string $haystack
     * @param string|array $needles
     * 
     * @return array|bool
     */
    public static function getContains(string $haystack, $needles) {
        foreach ((array) $needles as $needle) {
            if ($needle !== '' && mb_strpos($haystack, $needle) !== false) {
                return $needle;
            }
        }
        return null;
    }

    public static function parsePercentage(string $percentage): float {
        if(Str::contains($percentage, ",")) {
            $percentage = preg_replace("/[\,]/", ".", $percentage);
        }
        Str::of($percentage)->remove("%")->trim();
        return floatval($percentage);
    }

    /**
     * Parse a price from a string value
     * 
     * @param string $price
     * 
     * @return float
     */
    public static function parsePrice(string $price): array {
        $currency = self::getCurrency($price);
        $commaNumber = substr_count($price, ",");
        $dotNumber = substr_count($price, ".");
        $commaLastPos = mb_strripos($price, ",");
        $dotLastPos = mb_strripos($price, ".");
        $price = preg_replace("/[ ]/", "", $price);
        
        if($commaNumber && $dotNumber) {
            if($commaNumber === 1 && $commaLastPos > $dotLastPos) {
                $price = preg_replace("/[\.]/", "", $price);
                $price = preg_replace("/[\,]/", ".", $price);
            } else if($dotNumber === 1 && $dotLastPos > $commaLastPos) {
                $price = preg_replace("/[\,]/", "", $price);
            }
        } else if($commaNumber && !$dotNumber) {
            if($commaNumber === 1) {
                $price = preg_replace("/[\,]/", ".", $price);
            } else {
                $price = preg_replace("/[\,]/", "", $price);
            }
        } else if(!$commaNumber && $dotNumber) {
            if($dotNumber > 1) {
                $price = preg_replace("/[\.]/", "", $price);
            }
        }
        return ['price' => floatval($price), 'currency' => $currency];
    }
    
    /**
     * Get the currency of the price (predefined currencies set)
     * 
     * @param string $price
     * @return string
     */
    public static function getCurrency(string &$price) {
        foreach(Data::$currencies as $currency) {
            $contains = self::getContains(Str::lower($price), [
                Str::lower($currency['namePlural']),
                Str::lower($currency['name']),
                Arr::exists($currency, 'alternative') ? Str::lower($currency['alternative']) : "",
                Str::lower($currency['code']),
                $currency['symbol'],
                $currency['symbolNative'],
            ]);
            if($contains) {
                $price = Str::of($price)->lower()->remove(Str::of($contains)->lower())->trim();
                return $currency;
            }
        }
        return null;
    }

    /**
     * Get the part of the string after offset
     * 
     * @param int $offset
     * @param string $string
     * @return string
     */
    public static function getStringRightOffset(int $offset, string $string): string {
        $splittedString = self::str_split_unicode($string, 1);
        return self::joinBetweenKeys($splittedString, $offset, count($splittedString));
    }

    /**
     * Get the part of the string before offset
     * 
     * @param int $offset
     * @param string $string
     * @return string
     */
    public static function getStringLeftOffset(int $offset, string $string): string {
        $splittedString = self::str_split_unicode($string, 1);
        return self::joinBetweenKeys($splittedString, 0, $offset - 1);
    }

    /**
     * Merge array values contained between 2 keys in a string
     * 
     * @param array $array
     * @param int $start
     * @param int $end
     * @return string
     */
    public static function joinBetweenKeys(array $array, int $start, int $end): string {
        $string = '';
        foreach($array as $key => $item) {
            if($key === $end) {
                $string .= $item;
                return $string;
            }
            if($key >= $start) {
                $string .= $item;
            }
        }
        return $string;
    }

    /**
     * Re-index array key items from 0 to n
     * 
     * @param array $array
     * @return array
     */
    public static function reindexArray(array $array) {
        $newArray = [];
        foreach($array as $item) {
            $newArray[] = $item;
        }
        return $newArray;
    }

    /**
     * Check if string contains a price
     * 
     * @param string $string
     * @return array
     */
    public static function containsMoreNumbersThan(string $string, int $number): array {
        preg_match_all("/(?:[0-9](?:[\-\.\' ]{0,1})){". ($number + 1) .",}/", $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains a price
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsPrice(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['price']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains an inverted price
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsPriceInvert(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['price_invert']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains a price with a comma
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsPriceComma(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['price_comma']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains an inverted price with a comma
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsPriceCommaInvert(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['price_comma_invert']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains a price with a dot
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsPriceDot(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['price_dot']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains an inverted price with a dot
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsPriceDotInvert(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['price_dot_invert']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains a percentage
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsPercentage(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['percentage']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains an inverted percentage
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsPercentageInvert(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['percentage_invert']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains a number
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsNumber(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['number']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains a date
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsDate(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['date']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains a short date
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsDateShort(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['date_short']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains an inverted date
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsDateInvert(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['date_invert']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains a long date
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsDateLong(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['date_long']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains a name
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsName(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['name']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains an email
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsEmail(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['email']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains an url
     * @deprecated
     * 
     * @param string $string
     * 
     * @return array
     */
    public static function containsUrl(string $string): array {
        $matches = null;
        preg_match_all(Data::$regex['url']['pattern'], $string, $matches);
        return $matches[0];
    }

    /**
     * Check if string contains a certain pattern (defined in Data class)
     * 
     * @param string $string
     * @param string $pattern
     * 
     * @return array
     */
    public static function contains(string $string, string $pattern): array {
        $matches = null;
        preg_match_all($pattern, $string, $matches);
        return $matches[0];
    }
    
}
