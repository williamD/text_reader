<?php

namespace App\Http\Helpers;

class Performance {

    private $time_start;
    private $time_end;

    public function start() {
        $this->time_start = hrtime(true);
    }

    public function stop() {
        $this->time_end = hrtime(true);
    }

    public function getExecutionTime() {
        $result = $this->time_end - $this->time_start;
        if($result/1e+6 > 1000) {
            return [number_format(($result/1e+9), 3), 's'];
        } else {
            return [number_format(($result/1e+6), 4), 'ms'];
        }
    }

    public function getResult() {
        if(!$this->time_start) {
            throw new Exception('start method is not defined');
        } else if(!$this->time_end) {
            throw new Exception('stop method is not defined');
        } else {
            return $this->getExecutionTime()[0].' '.$this->getExecutionTime()[1];
        }
    }

}
