<?php

namespace App\Http\Helpers\NLP;

use Maatwebsite\Excel\Excel;
use Web64\LaravelNlp\Facades\NLP;
use Illuminate\Support\Facades\Storage;
use TeamTNT\TNTSearch\Classifier\TNTClassifier;

class Classifier {

    private $classifier;
    private $classifierName = "invoices.cls";

    public function __construct() {
        $this->classifier = new TNTClassifier();
    }

    public function process() {
        //$this->train();
        $this->test();
    }

    private function test() {
        $this->classifier->load($this->classifierName);
        $sentence = "29900 Concarneau";
        
        var_dump($this->classifier->predict($sentence));
    }

    private function train() {
        $this->learnCSV("addresses.csv", "address");
        $this->learnCSV("names.csv", "name");
        $this->classifier->save($this->classifierName);
    }

    private function learnCSV(string $path, string $category) {
        $row = 1;
        if (($handle = fopen(Storage::disk("datasets")->path($path), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $num = count($data);
                $row++;
                $this->classifier->learn($data[0], $category);
            }
            fclose($handle);
        }
    }

}
