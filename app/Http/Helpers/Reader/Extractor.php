<?php

namespace App\Http\Helpers\Reader;

use ArrayIterator;
use MultipleIterator;
use App\Http\Helpers\Data;
use App\Http\Helpers\Utils;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Extractor {

    private $headers = [];

    public function __construct() {}

    /**
     * Extract the header
     * 
     * @param int $headerLineNumber
     * @param array $splittedTextContent
     * @return array
     */
    public function extractHeader(array $headerLineNumbers, array $splittedTextContent): array {
        foreach($headerLineNumbers as $headerLineNumber) {
            $array = [];
            $finalArray = [];
            $matchedTermsGroups = [];
            $splittedHeader = Utils::splitBySpace(1, $splittedTextContent[$headerLineNumber]);
            $string = preg_replace("/ +/", " ", Str::of($splittedTextContent[$headerLineNumber])->trim()->lower());
            $stringNoSplit = Str::of($splittedTextContent[$headerLineNumber])->lower();
            foreach(Data::$header_data as $property) {
                //$contains = $this->containsProperty($string, $property['terms']);
                $matchedTerms = [];
                foreach($property['terms'] as $term) {
                    if(Str::contains($string, Str::lower($term))) {
                        $matchedTerms[] = $term;
                    }
                }
                if(!empty($matchedTerms)) {
                    usort($matchedTerms, "App\Http\Helpers\sortByLongestString");
                    $matchedTermsGroups[] = $matchedTerms;
                }
            }
            $maxMatchedTerms = [];
            foreach($matchedTermsGroups as $matchedTermsGroupsItem) {
                if(count($matchedTermsGroupsItem) > count($maxMatchedTerms)) {
                    $maxMatchedTerms = $matchedTermsGroupsItem;
                }
            }

            // create a multipleiterator to loop through all matchedTerms at the same time
            $iterator = new MultipleIterator(MultipleIterator::MIT_NEED_ANY);
            foreach($matchedTermsGroups as $matchedTermsGroupsItem) {
                $iterator->attachIterator(new ArrayIterator($matchedTermsGroupsItem));
            }

            foreach($iterator as $item) {
                foreach($item as $itemValue) {
                    $splittedTerm = preg_split("/ +/", $itemValue);
                    $formattedTerm = "";
                    foreach($splittedTerm as $splittedTermItemIndex => $splittedTermItem) {
                        if(Arr::exists($splittedTerm, $splittedTermItemIndex + 1)) {
                            $formattedTerm .= preg_quote($splittedTermItem)." +";
                        } else {
                            $formattedTerm .= preg_quote($splittedTermItem);
                        }
                    }
                    preg_match("/". $formattedTerm ."/", $stringNoSplit, $matches);
                    $value = $matches ? $matches[0] : null;
                    if($value) {
                        $array[] = ['term' => $itemValue, 'value' => $value];
                        $string = Str::of(str_replace($itemValue, "", $string))->trim();
                        $stringNoSplit = Str::of(str_replace($value, "", $stringNoSplit));
                    }
                }
            }

            // set header data
            $unformattedHeader = $this->definePropertiesPositions($splittedTextContent[$headerLineNumber], $array);
            // var_dump($this->header);
            // filter header data (matching terms)
            foreach($unformattedHeader as $headerItem) {
                foreach(Data::$header_data as $property) {
                    if($this->propertyMatch($headerItem['value'], $property['terms'])) {
                        $headerItem['property'] = $property;
                        $finalArray[] = $headerItem;
                    }
                }
            }
            // sort header by shortest poisitions
            usort($finalArray, 'App\Http\Helpers\SortByShortestPosition');
            // set filtered header data
            $this->addHeader($finalArray);
            
        }
        return $this->getHeaders();
    }

    /**
     * Extract lines under the header
     * 
     * @param int $headerLineNumber
     * @param array $splittedTextContent
     */
    public function extractLines(int $headerLineNumber, array $splittedTextContent, array $header, $nextHeaderLine) {
        $array = [];
        // splitted header by char
        $splittedHeaderChar = Utils::str_split_unicode($splittedTextContent[$headerLineNumber], 1);

        // loop on lines
        for($lineNumber = ($headerLineNumber + 1); $lineNumber <= count($splittedTextContent); $lineNumber++) {
            
            if($nextHeaderLine && $lineNumber === $nextHeaderLine) break;
            if(array_key_exists($lineNumber, $splittedTextContent)) {
                $splittedLineChar = Utils::str_split_unicode($splittedTextContent[$lineNumber], 1);
                $array[] = ['line' => $splittedTextContent[$lineNumber], 'columns' => $this->select($splittedLineChar, $header)];
            }
        }
        return $array;
    }

    /**
     * Check if the given property match a predefine term
     * 
     * @param string $property
     * @param array $terms
     * @return bool
     */
    protected function propertyMatch(string $property, array $terms): bool {
        foreach($terms as $term) {
            if(Str::of($property)->trim()->lower() == Str::of($term)->lower()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Select line items based on the header items positions, seleccting the end of the previous element and start of next element
     * (deliberately too large to be sure to not miss any word)
     * 
     * @param array $splittedLineChar
     * @return array
     */
    protected function select(array $splittedLineChar, array $header): array {
        $columns = [];
        foreach($header as $propertyIndex => $property) {
            if(array_key_exists($propertyIndex, $header)) {
                if($propertyIndex === 0) {
                    if(array_key_exists($propertyIndex + 1, $header)) {
                        $columns[] = $this->joinBetweenKeys($splittedLineChar, 0, $header[$propertyIndex + 1]['position']['start'] - 1);
                    } else {
                        $columns[] = $this->joinBetweenKeys($splittedLineChar, 0, $header[$propertyIndex]['position']['end'] - 1);
                    }
                } else if($propertyIndex === (count($header) - 1)) {
                    $columns[] = $this->joinBetweenKeys($splittedLineChar, $header[$propertyIndex - 1]['position']['end'], count($splittedLineChar));
                } else {
                    if(array_key_exists($propertyIndex + 1, $header)) {
                        $columns[] = $this->joinBetweenKeys($splittedLineChar, $header[$propertyIndex - 1]['position']['end'], $header[$propertyIndex + 1]['position']['start'] - 1);
                    } else {
                        $columns[] = $this->joinBetweenKeys($splittedLineChar, $header[$propertyIndex - 1]['position']['end'], $header[$propertyIndex]['position']['end'] - 1);
                    }
                }
            }
        }
        return $columns;
    }

    /**
     * Merge array values contained between 2 keys in a string
     * 
     * @param array $array
     * @param int $start
     * @param int $end
     * @return string
     */
    protected function joinBetweenKeys(array $array, int $start, int $end): string {
        $string = '';
        foreach($array as $key => $item) {
            if($key === $end) {
                $string .= $item;
                return $string;
            }
            if($key >= $start) {
                $string .= $item;
            }
        }
        return $string;
    }

    /**
     * Check if a string contains a property term and return the contained properties
     * 
     * @param string $string
     * @param array $terms
     * @return array
     */
    protected function containsProperty(string $string, array $terms): array {
        
        $contains = [];
        $finalContains = [];
        foreach($terms as $term) {
            if(Str::contains($string, Str::lower($term))) {
                return [$term];
            }
        }
        /*foreach($contains as $itemIndex => $item) {
            if($finalContains) {
                $isIn = false;
                foreach($finalContains as $finalItem) {
                    if(Str::contains($finalItem, $item)) {
                        $isIn = true;
                    }
                }
                if(!$isIn) {
                    $finalContains[] = $item;
                }
            } else {
                $finalContains[] = $item;
            }
        }
        return $finalContains;*/
        return $contains;
    }

    /**
     * Set a start and an end position for a given property in header
     * 
     * @param string $header
     * @param string $property
     * @return array
     */
    protected function definePropertyPosition(string &$header, string $property): array {
        $array = [];
        $array['start'] = mb_stripos($header, $property);
        $array['end'] = mb_stripos($header, $property) + Str::length($property);
        return $array;
    }

    /**
     * Define properties positions in the header
     * 
     * @param string $header
     * @param array $properties
     * @return array
     */
    protected function definePropertiesPositions(string $header, array $properties): array {
        $array = [];
        $header = Str::lower($header);
        foreach($properties as $key => $property) {
            $array[$key]['value'] = $property['term'];
            $array[$key]['position'] = $this->definePropertyPosition($header, $property['value']);
        }
        return $array;
    }

    /**
     * Get the header data
     * 
     * @return array
     */
    public function getHeaders(): array {
        return $this->headers;
    }

    /**
     *  Set the header data
     * 
     * @param array $header
     */
    public function setHeaders(array $headers): void {
        $this->headers = $headers;
    }

    public function addHeader(array $header): void {
        $this->headers[] = $header;
    }

}
