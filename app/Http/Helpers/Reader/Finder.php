<?php

namespace App\Http\Helpers\Reader;

use App\Http\Helpers\Data;
use App\Http\Helpers\Utils;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Finder {

    public function __construct() {}

    /**
     * Find the invoice data table header
     * 
     * @param array $splittedTextContent
     * @return int
     *      return the header line number
     */
    public function findHeader(array $splittedTextContent): array {
        $headerLine = -1;
        $splittedLine = [];
        $matches = [];

        if($splittedTextContent) {
            foreach($splittedTextContent as $lineNumber => $line) {
                // split line every 1 white space to make sure we not loose any word (sometimes, pdftotext does not set enough white spaces between fields)
                $splittedLine = Utils::splitBySpace(1, $line);

                foreach($splittedLine as $lineItemNumber => $lineItem) {
                    foreach(Data::$header_data as $property) {
                        if($this->propertyMatch($lineItem, $property['terms'])) {
                            $matches[$lineNumber][] = $lineItem;
                        }
                    }
                }
            }
            // select the line that has the best matches score
            $headerLines = $this->selectBestLines($matches);
        }
        if(!empty($headerLines)) {
            // return the headerline if it's a coherent value
            return $headerLines;
        } else {
            return [];
        }
    }

    /**
     * Select the line with the most matches
     * 
     * @param array $matches
     * @return int
     *      return the line number
     */
    protected function selectBestLines(array $matches): array {
        $maxValue = 0;
        $bestLines = [];

        foreach($matches as $lineMatch) {
            $count = count($lineMatch);
            if($count > $maxValue) {
                $maxValue = $count;
            }
        }
        
        $bestLines = Arr::where($matches, function ($value, $key) use($maxValue) {
            return count($value) === $maxValue;
        });

        return array_keys($bestLines);
    }

    /**
     * Check if the given property match a predefine term
     * 
     * @param string $property
     * @param array $terms
     * @return bool
     */
    protected function propertyMatch(string $property, array $terms): bool {
        foreach($terms as $term) {
            if(Str::of($property)->lower()->trim() == Str::of($term)->trim()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Find datatables range (starting and ending lines in invoice)
     * 
     * @param array $splittedTextContent
     * @param int $headerLineNumber
     * @param array $finalLines
     * 
     * @return array
     */
    public function findDataTableRange(array $splittedTextContent, int $headerLineNumber, array $finalLines): array {
        $datatableStart = null;
        $datatableEnd = null;
        $max = 0;
        $lastLine = null;
        $lastFinalLineItemArrays = [];
        $lastFinalLine = Arr::last($finalLines);
        $datatableStart = $headerLineNumber;
        
        foreach($lastFinalLine['columns'] as $lastFinalLineItem) {
            if(is_array($lastFinalLineItem)) {
                $lastFinalLineItemArrays[] = $lastFinalLineItem;
            }
        }
        if(!empty($lastFinalLineItemArrays)) {
            $lastLine = Arr::last(Arr::last($lastFinalLineItemArrays));
            foreach($splittedTextContent as $lineIndex => $line) {
                if(Str::contains($line, $lastLine)) {
                    if($headerLineNumber >= $lineIndex) {
                        continue;
                    } else {
                        $datatableEnd = $lineIndex;
                        break;
                    }
                }
            } 
        } else {
            foreach($splittedTextContent as $lineIndex => $line) {
                if(Str::contains($line, $lastFinalLine['line'])) {
                    $datatableEnd = $lineIndex;
                    break;
                }
            }
        }
        if(!$datatableEnd) {
            end($splittedTextContent);
            $datatableEnd = key($splittedTextContent);
        }
        return ['start' => $datatableStart, 'end' => $datatableEnd];
    }

    public function findTotalLabels(array $splittedTextContent): array {
        $patterns = [];
        $results = [];
        foreach(Data::$total as $property) {
            $propertySplitted = Utils::splitBySpace(1, preg_quote(Str::lower($property), "/"));
            $patterns[] = implode(" *", $propertySplitted);
        }
        foreach($splittedTextContent as $lineIndex => $line) {
            foreach($patterns as $pattern) {
                preg_match_all("/(?<![^\s:\.,;=_])". $pattern ."(?![^\s:\.,;=_])/", Str::lower($line), $matches);
                if($matches[0]) {
                    foreach($matches[0] as $match) {
                        $results[$lineIndex][] = $match;
                    }
                }
            }
        }
        return $results;
    }

}
