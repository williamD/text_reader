<?php

namespace App\Http\Helpers\Reader;

use App\Http\Helpers\Data;
use App\Http\Helpers\Utils;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Lines {

    private $eol = null;

    private $datatableStart = null;
    private $datatableEnd = null;

    public function __construct() {}

    /**
     * Retrieve lines from data table
     * 
     * @param array $lines
     * @param array $header
     * @return array
     */
    public function getLines(array $lines, array $header): array {
        $linesStepLabel = [];
        $linesStepMerge = [];
        $previousSavedLineIndex = null;
        $previousSavedLineMergeIndex = null;

        // get the pattern
        $pattern = $this->getLinesPattern($lines, $header);
        foreach($lines as $line) {
            // remove white spaces before and after line items
            $line['columns'] = $this->trimLine($line['columns']);
            $refactoredLine = $this->refactorLine($linesStepLabel[$previousSavedLineIndex], $line, $pattern, $header);
            if(!$this->isEmptyLine($refactoredLine['content']['columns'])) {
                $linesStepLabel[] = $refactoredLine;
                if($refactoredLine['match']) {
                    end($linesStepLabel);
                    $previousSavedLineIndex = key($linesStepLabel);
                }
            }
        }

        $eol = $this->endOfLines($linesStepLabel, $pattern, $header);
        $this->eol = $eol;
        $linesStepLabel[$eol]['eol'] = true;

        $previousLineIndex = null;
        foreach($linesStepLabel as $lineIndex => $line) {
            if($line === null) {
                unset($line);
                continue;
            }
            $refactoredLine = $this->mergeMultiline($linesStepMerge[$previousSavedLineMergeIndex], $line, $pattern, $header);
            //dump($refactoredLine);
            //var_dump($linesStepMerge[$previousSavedLineMergeIndex]);
            if($refactoredLine === null) {
                break;
            }
            if(!$this->isEmptyLine($refactoredLine['content']['columns'])) {
                $linesStepMerge[] = $refactoredLine;
                end($linesStepMerge);
                $previousSavedLineMergeIndex = key($linesStepMerge);
            }
        }
        foreach($linesStepMerge as $lineStepMergeIndex => $lineStepMerge) {
            if($lineStepMerge !== null) {
                if(!$this->hasPrice($lineStepMerge, $pattern, $header) && !$this->hasQuantity($lineStepMerge, $pattern, $header)) {
                    unset($linesStepMerge[$lineStepMergeIndex]);
                }
            }
        }
        //var_dump($linesStepLabel);
        //dump($linesStepMerge);
        $this->filterLines($linesStepMerge, $pattern, $header);
        $prices = $this->collectPrices($linesStepMerge, $pattern, $header);
        //dump($linesStepMerge);
        return ['lines' => array_column($linesStepMerge, 'content'), 'prices' => $prices];
    }

    /**
     * Get lines pattern
     * 
     * @param array $lines
     * @param array $header
     * @return array|null
     */
    public function getLinesPattern(array $lines, array $header) {
        $linesCopy = $lines;
        $completeLines = [];
        $uncompleteLines = [];
        $correspondingFormatsArray = [];
        $correspondingFormatsNumberArray = [];
        $pattern = null;

        foreach($linesCopy as $line) {
            // remove white spaces before and after line items
            $line = $this->trimLine($line['columns']);
            // check if line does not contains any blank item
            if($this->isCompleteLine($line)) {
                $completeLines[] = $line;
            } else if($this->isUncompleteLine($line)) {
                $uncompleteLines[] = $line;
            }
        }
        // get corresponding formats by complete line
        if($completeLines) {
            foreach($completeLines as $completeLineIndex => $completeLine) {
                $correspondingFormatsArray[] = $this->getCorrespondingFormats($completeLine, $header);
            }
        } else if($uncompleteLines) {
            foreach($uncompleteLines as $uncompleteLineIndex => $uncompleteLine) {
                $correspondingFormatsArray[] = $this->getCorrespondingFormats($uncompleteLine, $header);
            }
        }
        // get number of corresponding formats by complete line
        foreach($correspondingFormatsArray as $correspondingFormatsIndex => $correspondingFormats) {
            $correspondingFormatsNumberArray[$correspondingFormatsIndex] = 0;
            foreach($correspondingFormats as $correspondingFormat) {
                if($correspondingFormat) {
                    $correspondingFormatsNumberArray[$correspondingFormatsIndex]++;
                }
            }
        }
        // select and return the pattern having the maximum corresponding formats number
        $pattern =  $correspondingFormatsArray[array_keys($correspondingFormatsNumberArray, max($correspondingFormatsNumberArray))[0]];
        return $pattern;
    }

    private function filterLines(array &$lines, array $pattern, array $header) {
        foreach($lines as $lineIndex => $line) {
            if($line) {
                if(!$this->isEmptyLine($line['content']['columns'])) {
                    if(!Arr::exists($line, 'multiline')) {
                        $mismatch = false;
                        foreach($line['content']['columns'] as $lineItemIndex => $lineItem) {
                            if($pattern[$lineItemIndex] === 'price') {
                                // dump([$lineItem, $this->isPrice($lineItem)]);
                                if(!$this->isPrice($lineItem)) {
                                    $mismatch = true;
                                    break;
                                }
                            }
                        }
                        //dump(['pattern' => $pattern, 'line' => $line['content']['columns'], 'notmismatch' => !$mismatch, 'mismatchvalue' => $mismatchValue, 'hasminrequired' => $this->hasMinRequired($line, $pattern, $header)]);
                        if(!$this->hasMinRequired($line, $pattern, $header) || $mismatch) {
                            unset($lines[$lineIndex]);
                        }
                    }
                }
            }
        }
    }

    /**
     * Find the end of data table lines
     * 
     * @param array $lines
     * @param array $pattern
     * @return int
     */
    private function endOfLines(array $lines, array $pattern, array $header): int {
        $endOfLines = -1;
        $hasPrevious = false;
        /*foreach($lines as $lineIndex => &$line) {
            if($line) {
                if(!$this->isEmptyLine($line['content']) && $hasPrevious) {
                    if(!Arr::exists($line, 'multiline')) {
                        if(!$this->hasMinRequired($line, $pattern, $header)) {
                            return $lineIndex;
                        }
                        foreach($line['content'] as $lineItemIndex => $lineItem) {
                            if($pattern[$lineItemIndex] === 'price' && !$this->checkPrice($lineItem)) {
                                return $lineIndex;
                            }
                        }
                    }
                } else {
                    $hasPrevious = true;
                }
            }
        }*/
        return $endOfLines;
    }

    /**
     * Check if line has the minimum items required to be extracted
     * 
     * @param array $line
     * @param array $pattern
     * @param array $header
     * @return bool
     */
    public function hasMinRequired(array $line, array $pattern, array $header): bool {
        $hasQuantity = false;
        $hasPrice = false;
        $hasObject = false;
        $hasReference = false;

        foreach($header as $headerItem) {
            if($headerItem['property']['name'] === 'reference') {
                $hasReference = true;
            }
            if($headerItem['property']['name'] === 'object') {
                $hasObject = true;
            }
            if($headerItem['property']['name'] === 'quantity') {
                $hasQuantity = true;
            }
            if($headerItem['property']['format'] === 'price') {
                $hasPrice = true;
            }
        }
        
        if(!$hasObject && !$hasReference) {
            return false;
        }
        if($hasObject && $hasReference) {
            $emptyReference = false;
            $emptyObject = false;
            foreach($line['content']['columns'] as $lineItemIndex => $lineItem) {
                if($header[$lineItemIndex]['property']['name'] === 'object' && blank($lineItem)) {
                    $emptyObject = true;
                }
                if($header[$lineItemIndex]['property']['name'] === 'reference' && blank($lineItem)) {
                    $emptyReference = true;
                }
            }
            if($emptyObject && $emptyReference) {
                return false;
            }
        }
        if($hasObject && !$hasReference) {
            $emptyObject = false;
            foreach($line['content']['columns'] as $lineItemIndex => $lineItem) {
                if($header[$lineItemIndex]['property']['name'] === 'object' && blank($lineItem)) {
                    $emptyObject = true;
                }
            }
            if($emptyObject) {
                return false;
            }
        }
        if(!$hasObject && $hasReference) {
            $emptyReference = false;
            foreach($line['content']['columns'] as $lineItemIndex => $lineItem) {
                if($header[$lineItemIndex]['property']['name'] === 'reference' && blank($lineItem)) {
                    $emptyReference = true;
                }
            }
            if($emptyReference) {
                return false;
            }
        }
        if($hasQuantity && $hasPrice) {
            $countEmptyPrices = 0;
            $emptyQuantity = false;
            foreach($line['content']['columns'] as $lineItemIndex => $lineItem) {
                if($header[$lineItemIndex]['property']['name'] === 'quantity' && blank($lineItem)) {
                    $emptyQuantity = true;
                }
                if($header[$lineItemIndex]['property']['format'] === 'price' && blank($lineItem)) {
                    $countEmptyPrices++;
                }
            }
            if($emptyQuantity && $countEmptyPrices === 0) {
                return true;
            } else if($emptyQuantity && $countEmptyPrices > 0) {
                return false;
            } else if(!$emptyQuantity && $countEmptyPrices === 0) {
                return true;
            } else {
                return false;
            }
        } else if(!$hasQuantity && $hasPrice) {
            $countEmptyPrices = 0;
            foreach($line['content']['columns'] as $lineItemIndex => $lineItem) {
                if($header[$lineItemIndex]['property']['format'] === 'price' && blank($lineItem)) {
                    $countEmptyPrices++;
                }
            }
            if($countEmptyPrices > 0) {
                return false;
            } else {
                return true;
            }
        } else if($hasQuantity && !$hasPrice) {
            $emptyQuantity = false;
            foreach($line['content']['columns'] as $lineItemIndex => $lineItem) {
                if($header[$lineItemIndex]['property']['name'] === 'quantity' && blank($lineItem)) {
                    $emptyQuantity = true;
                }
            }
            if($emptyQuantity) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * Check if line contains any quantity
     * 
     * @param array $line
     * @param array $pattern
     * @param array $header
     * @return bool
     */
    private function hasQuantity(array $line, array $pattern, array $header): bool {
        $has = false;
        foreach($line['content']['columns'] as $lineItemIndex => $lineItem) {
            if($header[$lineItemIndex]['property']['name'] === 'quantity') {
                if($this->getLineItemFormat($lineItem, $pattern[$lineItemIndex]) === 'number') {
                    $has = true;
                }
            }
        }
        return $has;
    }

    /**
     * Check if line contains any price value
     * 
     * @param array $line
     * @param array $pattern
     * @param array $header
     * @return bool
     */
    private function hasPrice(array $line, array $pattern, array $header): bool {
        $has = false;
        foreach($line['content']['columns'] as $lineItemIndex => $lineItem) {
            if(Str::contains($header[$lineItemIndex]['property']['name'], array_column(array_filter(Data::$header_data, function($value) {
                return $value['format'] == 'price';
            }), 'name'))) {
                if($this->getLineItemFormat($lineItem, $pattern[$lineItemIndex]) === 'price') {
                    $has = true;
                }
            }
        }
        return $has;
    }

    /**
     * Check if line item not matching pattern
     * 
     * @param int $index
     * @param string $lineItem
     * @param array $pattern
     * @return bool
     */
    private function hasMismatch(int $index, string $lineItem, array $pattern): bool {
        if($pattern[$index] === 'string' || $pattern[$index] == null) {
            return false;
        } else if($this->getLineItemFormat($lineItem, $pattern[$index]) === $pattern[$index]) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Refactor the line
     * 
     * @param array|null $previousSavedLine
     * @param array $line
     * @param array $pattern
     * @param array $header
     * @return array|null
     *      return the formatted line
     */
    private function refactorLine(&$previousSavedLine, array $line, array $pattern, array $header) {
        $newLine = false;
        $formattedLine = $this->keepMatchingValues($line, $pattern);
        // if line is not marked as match and has a previous line
        if(!$formattedLine['match'] && $previousSavedLine) {
            $this->removeDuplicateParts($formattedLine['content']['columns'], $pattern, $header);
            foreach($formattedLine['content']['columns'] as $formattedLineItemIndex => $formattedLineItem) {
                if(!$header[$formattedLineItemIndex]['property']['multiline'] && !empty($formattedLineItem)) {
                    $newLine = true;
                }
            }
            if(!$newLine) {
                // if not a new line, mark as multiline
                foreach($previousSavedLine['content']['columns'] as $previousSavedLineItemIndex => &$previousSavedLineItem) {
                    if(!empty($formattedLine['content']['columns'][$previousSavedLineItemIndex])) {
                        $formattedLine['multiline'] = true;
                    }
                }
            } 
        }
        $formattedLine = $this->removeUnwantedValuesInObject($formattedLine, $header);
        //var_dump($formattedLine);
        //dump($formattedLine);
        
        return $formattedLine;
    }

    /**
     * Merge lines which have values on multiple lines
     * 
     * @param array|null $previousSavedLine
     * @param array $line
     * @param array $pattern
     * @param array $header
     * @return array|null
     */
    private function mergeMultiline(&$previousSavedLine, array &$line, array $pattern, array $header) {
        //$line = $this->selectCorrectValue($line, $pattern, $header);
        // if line marked as multiline, need to merge with previous line
        if(array_key_exists('eol', $line)) {
            return null;
        }

        if(array_key_exists('multiline', $line)) {
            foreach($previousSavedLine['content']['columns'] as $previousSavedLineItemIndex => &$previousSavedLineItem) {
                if(!empty($line['content']['columns'][$previousSavedLineItemIndex])) {
                    if(!is_array($previousSavedLineItem)) {
                        $save = $previousSavedLineItem;
                        $previousSavedLineItem = [];
                        $previousSavedLineItem[] = $save;
                    }
                    $previousSavedLineItem[] = $line['content']['columns'][$previousSavedLineItemIndex];
                    //$line['content']['columns'] = [];
                    $line['content']['columns'][$previousSavedLineItemIndex] = '';
                }
                
            }
        }
        // select correct quantity value
        $line = $this->selectCorrectValue($line, $pattern, $header);
        return $line;
    }

    public function otherItemsEmpty($line, $itemIndex) {
        $empty = true;
        foreach($line['content']['columns'] as $lineItemIndex => $lineItem) {
            if($lineItemIndex !== $itemIndex) {
                if(!blank($lineItem)) {
                    return false;
                }
            }
        }
        return $empty;
    }

    private function removeDuplicateParts2(array &$line, array $pattern): void {
        foreach($line as $lineItemIndex => &$lineItem) {

        }
    }

    /**
     * Remove duplicate parts in line items
     * 
     * @param array $line
     * @param array $pattern
     * @return void
     */
    private function removeDuplicateParts(array &$line, array $pattern, array $header): void {
        $lineCopy = $line;
        foreach($line as $lineItemIndex => &$lineItem) {
            // if not last item
            if(Arr::exists($line, $lineItemIndex + 1)) {
                if(Str::contains($lineItem, $line[$lineItemIndex + 1])) {
                    //dump([$lineItem, $line[$lineItemIndex + 1]]);
                    $notCorrect = [];
                    // if pattern does not match the line item format add the line item index to the notCorrect array
                    if($pattern[$lineItemIndex] !== $this->getLineItemFormat($lineItem, $pattern[$lineItemIndex])) {
                        $notCorrect[] = $lineItemIndex;
                    }
                    if($pattern[$lineItemIndex + 1] !== $this->getLineItemFormat($line[$lineItemIndex + 1], $pattern[$lineItemIndex + 1])) {
                        $notCorrect[] = $lineItemIndex + 1;
                    }
                    if($notCorrect) {
                        // remove notCorrectValue from line
                        foreach($notCorrect as $notCorrectValue) {
                            $line[$notCorrectValue] = "";
                        }
                    } else {
                        $line[$lineItemIndex + 1] = "";
                    }
                } else if(Str::contains($line[$lineItemIndex + 1], $lineItem)) {
                    $notCorrect = [];
                    if($pattern[$lineItemIndex] !== $this->getLineItemFormat($lineItem, $pattern[$lineItemIndex])) {
                        $notCorrect[] = $lineItemIndex;
                    }
                    if($pattern[$lineItemIndex + 1] !== $this->getLineItemFormat($line[$lineItemIndex + 1], $pattern[$lineItemIndex + 1])) {
                        $notCorrect[] = $lineItemIndex + 1;
                    }
                    if($notCorrect) {
                        foreach($notCorrect as $notCorrectValue) {
                            $line[$notCorrectValue] = "";
                        }
                    } else {
                        $lineItem = "";
                    }
                }
            }
            // if not first item
            if(Arr::exists($line, $lineItemIndex - 1)) {
                if(Str::contains($lineItem, $line[$lineItemIndex - 1])) {
                    $notCorrect = [];
                    if($pattern[$lineItemIndex] !== $this->getLineItemFormat($lineItem, $pattern[$lineItemIndex])) {
                        $notCorrect[] = $lineItemIndex;
                    }
                    if($pattern[$lineItemIndex - 1] !== $this->getLineItemFormat($line[$lineItemIndex - 1], $pattern[$lineItemIndex - 1])) {
                        $notCorrect[] = $lineItemIndex - 1;
                    }
                    if($notCorrect) {
                        foreach($notCorrect as $notCorrectValue) {
                            $line[$notCorrectValue] = "";
                        }
                    } else {
                        $line[$lineItemIndex - 1] = "";
                    }
                } else if(Str::contains($line[$lineItemIndex - 1], $lineItem)) {
                    $notCorrect = [];
                    if($pattern[$lineItemIndex] !== $this->getLineItemFormat($lineItem, $pattern[$lineItemIndex])) {
                        $notCorrect[] = $lineItemIndex;
                    }
                    if($pattern[$lineItemIndex - 1] !== $this->getLineItemFormat($line[$lineItemIndex - 1], $pattern[$lineItemIndex - 1])) {
                        $notCorrect[] = $lineItemIndex - 1;
                    }
                    if($notCorrect) {
                        foreach($notCorrect as $notCorrectValue) {
                            $line[$notCorrectValue] = "";
                        }
                    } else {
                        $lineItem = "";
                    }
                }
            }
        }
    }

    private function keepMatchingValues2(array $line, array $pattern): array {
        $lineCopy = $line;
        $match = true;
        foreach($lineCopy['columns'] as $lineItemIndex => &$lineItem) {
            if(Arr::exists($lineCopy['columns'], $lineItemIndex + 1)) {
                
            }
            if(Arr::exists($lineCopy['columns'], $lineItemIndex - 1)) {
                if(preg_match("/". preg_quote($lineItem, '/') ."/", $lineCopy['columns'][$lineItemIndex - 1], $matches)) {
                    if($matches[0]) {
                        $match = false;
                        dump($matches[0]);
                        $lineItem = Str::remove($lineItem, $lineItem);
                    }
                }
            }
        }
        return [
            'match' => $match,
            'content' => $lineCopy,
        ];
    }

    /**
     * Check if line items values are corresponding with formats and merge them if multiple
     * 
     * @param array $line
     * @param array $pattern
     * @return array
     */
    private function keepMatchingValues(array $line, array $pattern): array {
        $lineCopy = $line;
        $match = true;
        foreach($lineCopy['columns'] as $lineItemIndex => &$lineItem) {
            
            switch($pattern[$lineItemIndex]) {
                case 'number':
                    if(!blank($lineItem)) {
                        $value = Utils::contains($lineItem, Data::$regex['number']['pattern']);
                        if(!$value) {
                            $match = false;
                        } else {
                            $lineItem = implode('   ', Arr::flatten($value));
                        }
                    }
                    break;
                case 'percentage':
                    if($contains = Utils::contains($lineItem, Data::$regex['percentage_invert']['pattern'])) {
                        $lineItem = implode('   ', Arr::flatten($contains));
                    } else if($contains = Utils::contains($lineItem, Data::$regex['percentage']['pattern'])) {
                        $lineItem = implode('   ', Arr::flatten($contains));
                    } else {
                        $match = false;
                    }
                    break;
                case 'price':
                    if($contains = Utils::contains($lineItem, Data::$regex['price_dot_w']['pattern'])) {
                        $lineItem = implode('   ', Arr::flatten($contains));
                    } else if($contains = Utils::contains($lineItem, Data::$regex['price_comma_w']['pattern'])) {
                        $lineItem = implode('   ', Arr::flatten($contains));
                    } else {
                        $match = false;
                    }
                    break;
                case 'date':
                    if($contains = Utils::contains($lineItem, Data::$regex['date_long']['pattern'])) {
                        $lineItem = implode('   ', Arr::flatten($contains));
                    } else if($contains = Utils::contains($lineItem, Data::$regex['date']['pattern'])) {
                        $lineItem = implode('   ', Arr::flatten($contains));
                    } else if($contains = Utils::contains($lineItem, Data::$regex['date_invert']['pattern'])) {
                        $lineItem = implode('   ', Arr::flatten($contains));
                    } else if($contains = Utils::contains($lineItem, Data::$regex['date_short']['pattern'])) {
                        $lineItem = implode('   ', Arr::flatten($contains));
                    } else {
                        $match = false;
                    }
                    break;
            }
        }
        // var_dump([
        //     'match' => $match,
        //     'content' => $lineCopy,
        // ]);
        //dump([$line, $lineCopy]);
        return [
            'match' => $match,
            'content' => $lineCopy,
        ];
    }

    /**
     * Remove white spaces before and after line items
     * 
     * @param array $line
     * @return array
     *      return the trimed line
     */
    private function trimLine(array $line) {
        $lineCopy = $line;
        foreach($lineCopy as &$lineItem) {
            $lineItem = trim($lineItem);
        }
        return $lineCopy;
    }

    /**
     * Check if the line is complete
     * 
     * @param array $line
     * @return bool
     */
    private function isCompleteLine(array $line) {
        $complete = true;
        foreach($line as $lineItem) {
            if(empty($lineItem)) {
                $complete = false;
            }
        }
        return $complete;
    }

    /**
     * Check if the line is uncomplete
     * 
     * @param array $line
     * @return bool
     */
    private function isUncompleteLine(array $line) {
        $uncomplete = false;
        foreach($line as $lineItem) {
            if(!empty($lineItem)) {
                $uncomplete = true;
            }
        }
        return $uncomplete;
    }

    /**
     * Check if the line is empty
     * 
     * @param array $line
     * @return bool
     */
    private function isEmptyLine(array $line): bool {
        $empty = true;
        foreach($line as $lineItem) {
            if(!empty($lineItem)) {
                $empty = false;
            }
        }
        return $empty;
    }

    /**
     * Get line format
     * 
     * @param array $line
     * @param array $header
     * @return array
     */
    private function getCorrespondingFormats(array $line, array $header): array {
        $correspondingFormats = [];
        foreach($line as $lineItemIndex => $lineItem) {
            $correspondingFormats[] = $this->getLineItemFormat($lineItem, $header[$lineItemIndex]['property']['format']);
        }
        return $correspondingFormats;
    }

    /**
     * Define the format of the line item based on its content using regex
     * 
     * @param string $lineItem
     * @param string $headerFormat
     * @return string
     *      return the format
     */
    private function getLineItemFormat(string $lineItem, string $headerFormat): string {
        $format = 'string';
        if(Utils::contains($lineItem, Data::$regex['number']['pattern']) && $headerFormat == 'number') {
            $format = 'number';
        }
        if(Utils::contains($lineItem, Data::$regex['percentage_invert']['pattern']) && $headerFormat == 'percentage') {
            $format = 'percentage';
        }
        if(Utils::contains($lineItem, Data::$regex['percentage']['pattern']) && $headerFormat == 'percentage') {
            $format = 'percentage';
        }
        if(Utils::contains($lineItem, Data::$regex['price_comma_w']['pattern']) && $headerFormat == 'price') {
            $format = 'price';
        }
        if(Utils::contains($lineItem, Data::$regex['price_dot_w']['pattern']) && $headerFormat == 'price') {
            $format = 'price';
        }
        if(Utils::contains($lineItem, Data::$regex['date_short']['pattern']) && $headerFormat == 'date') {
            $format = 'date';
        } 
        if(Utils::contains($lineItem, Data::$regex['date_invert']['pattern']) && $headerFormat == 'date') {
            $format = 'date';
        }
        if(Utils::contains($lineItem, Data::$regex['date']['pattern']) && $headerFormat == 'date') {
            $format = 'date';
        }
        if(Utils::contains($lineItem, Data::$regex['date_long']['pattern']) && $headerFormat == 'date') {
            $format = 'date';
        }
        return $format;
    }



    /**
     * Remove duplicate values in the object field
     * 
     * @param array $line
     * @param array $header
     * @return array
     */
    private function removeUnwantedValuesInObject(array $line, array $header) {
        $lineCopy = $line;
        foreach($lineCopy['content']['columns'] as $lineItemIndex => &$lineItem) {
            if($header[$lineItemIndex]['property']['name'] === 'object') {
                if($contains = Utils::contains($lineItem, Data::$regex['date_long']['pattern'])) {
                    if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['date_long']['pattern']) || Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['date_long']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && !array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['date_long']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(!array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['date_long']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    }
                } else if($contains = Utils::contains($lineItem, Data::$regex['date']['pattern'])) {
                    if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['date']['pattern']) || Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['date']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && !array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['date']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(!array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['date']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    }
                } else if($contains = Utils::contains($lineItem, Data::$regex['date_invert']['pattern'])) {
                    if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['date_invert']['pattern']) || Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['date_invert']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && !array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['date_invert']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(!array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['date_invert']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    }
                } else if($contains = Utils::contains($lineItem, Data::$regex['date_short']['pattern'])) {
                    if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['date_short']['pattern']) || Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['date_short']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && !array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['date_short']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(!array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['date_short']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    }
                } else if($contains = Utils::contains($lineItem, Data::$regex['price_dot_w']['pattern'])) {
                    if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['price_dot_w']['pattern']) || Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['price_dot_w']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && !array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['price_dot_w']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(!array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['price_dot_w']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    }
                } else if($contains = Utils::contains($lineItem, Data::$regex['price_comma_w']['pattern'])) {
                    if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['price_comma_w']['pattern']) || Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['price_comma_w']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && !array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex - 1], Data::$regex['price_comma_w']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    } else if(!array_key_exists($lineItemIndex - 1, $lineCopy['content']['columns']) && array_key_exists($lineItemIndex + 1, $lineCopy['content']['columns'])) {
                        if(Utils::contains($lineCopy['content']['columns'][$lineItemIndex + 1], Data::$regex['price_comma_w']['pattern'])) {
                            $lineCopy['content']['columns'][$lineItemIndex] = trim(Str::replaceFirst($contains[0], '', $lineItem));
                        }
                    }
                }
            }
        }
        return $lineCopy;
    }

    /**
     * Replace quantity item by the correct value
     * 
     * @param array $formattedLine
     * @param array $pattern
     * @param array $header
     * @return array
     */
    private function selectCorrectValue(array &$formattedLine, array $pattern, array $header): array {
        $taxes = Data::$tva;
        $prices = [];
        $amounts = [];
        $correctQuantityIndex = null;
        $quantityIndex = null;
        $formattedLineCopy = $formattedLine;
        $finalQuantity = 1;

        // check if line contains some prices
        if($this->hasPrice($formattedLineCopy, $pattern, $header)) {
            foreach($formattedLineCopy['content']['columns'] as $lineItemIndex => $lineItem) {
                // add tax in taxes array if found and is a number
                if($header[$lineItemIndex]['property']['name'] === 'tax') {
                    if($contains = Utils::contains($lineItem, Data::$regex['number']['pattern'])) {
                        $taxes[] = round(Utils::floatvalue($contains[0]), 2);
                    }
                }
                // add price in prices array
                if($header[$lineItemIndex]['property']['format'] === 'price' && $header[$lineItemIndex]['property']['name'] !== 'amount') {
                    $prices[] = round(Utils::floatvalue($lineItem), 2);
                }
                // add amount in amounts array
                if($header[$lineItemIndex]['property']['name'] === 'amount') {
                    $amounts[] = round(Utils::floatvalue($lineItem), 2);
                }
                if($header[$lineItemIndex]['property']['name'] === 'quantity') {
                    // save quantity index
                    $quantityIndex = $lineItemIndex;
                    $splittedLineItem = Utils::splitBySpace(3, $lineItem);
                    $splittedLineItemFloat = [];
                    // convert quantities to float values
                    foreach($splittedLineItem as $splittedLineItemValue) {
                        if($contains = Utils::contains($splittedLineItemValue, Data::$regex['number']['pattern'])) {
                            $splittedLineItemFloat[] = Utils::floatvalue($contains[0]);
                        }
                    }
                }
                if($header[$lineItemIndex]['property']['name'] === 'discount') {
                    $discountIndex = $lineItemIndex;
                    $splittedLineItem = Utils::splitBySpace(3, $lineItem);
                    $splittedLineItemFloatDiscount = [];

                    foreach($splittedLineItem as $splittedLineItemValue) {
                        if($contains = Utils::contains($splittedLineItemValue, Data::$regex['number']['pattern'])) {
                            $splittedLineItemFloatDiscount[] = Utils::floatvalue($contains[0]);
                        }
                    }
                }
            }
            if(isset($splittedLineItemFloat)) {
                if(count($splittedLineItemFloat) > 1) {
                    // if multiple quantities found, select the correct one and replace the line item
                    $correctQuantityIndex = $this->selectCorrectQuantity($splittedLineItemFloat, $taxes, $prices, $amounts);
                    if($correctQuantityIndex && $quantityIndex) {
                        $formattedLineCopy['content']['columns'][$quantityIndex] = $splittedLineItemFloat[$correctQuantityIndex];
                        $finalQuantity = $splittedLineItemFloat[$correctQuantityIndex];
                    }
                }
            }
            if(isset($splittedLineItemFloatDiscount)) {
                if(count($splittedLineItemFloatDiscount) > 1) {
                    $correctDiscountIndex = $this->selectCorrectDiscount($splittedLineItemFloatDiscount, $taxes, $prices, $amounts, $finalQuantity);
                    if($correctDiscountIndex && $discountIndex) {
                        $formattedLineCopy['content']['columns'][$discountIndex] = $splittedLineItemFloatDiscount[$correctDiscountIndex];
                    }
                }
            }
        }
        //var_dump($formattedLineCopy);
        return $formattedLineCopy;
    }

    /**
     * Select the correct quantity between multiple values
     * 
     * @param array $quantities
     * @param array $taxes
     * @param array $prices
     * @param array $amounts
     * @return int|null
     *      return the correct quantity index in array or null if not found
     */
    private function selectCorrectQuantity(array $quantities, array $taxes, array $prices, array $amounts) {
        $correctQuantity = null;
        $correctQuantityIndex = null;

        foreach($quantities as $quantityIndex => $quantity) {
            foreach($taxes as $tax) {
                foreach($prices as $price) {
                    if($correctQuantity) {
                        break 3;
                    }
                    // calculate all possible values between $taxes, $prices and $quantities until it match an amount value
                    $calc = round((((1/100) * $tax) + 1) * $price * $quantity, 2);
                    if(in_array($calc, $amounts)) {
                        $correctQuantityIndex = $quantityIndex;
                    }
                }
            }
        }
        return $correctQuantityIndex;
    }

    /**
     * Select the correct discount value between multiple values
     * 
     * @param array $discounts
     * @param array $taxes
     * @param array $prices
     * @param array $amounts
     * @return int|null
     *      return the correct discount index in array or null if not found
     */
    private function selectCorrectDiscount(array $discounts, array $taxes, array $prices, array $amounts, $quantity) {
        $correctDiscount = null;
        $correctDiscountIndex = null;

        foreach($discounts as $discountIndex => $discount) {
            foreach($taxes as $tax) {
                foreach($prices as $price) {
                    if($correctDiscount) {
                        break 3;
                    }
                    // calculate all possible values between $taxes, $prices and $quantities until it match an amount value
                    $calcBase = (((1/100) * $tax) + 1) * $price * $quantity;
                    $calcFlat = round($calcBase - $discount, 2);
                    $calcPercentage = round($calcBase - $calcBase * $discount/100, 2);
                    if(in_array($calcFlat, $amounts)) {
                        $correctDiscountIndex = $discountIndex;
                    }
                    if(in_array($calcPercentage, $amounts)) {
                        $correctDiscountIndex = $discountIndex;
                    }
                }
            }
        }
        return $correctDiscountIndex;
    }

    private function isPrice(string &$lineItem) {
        if(Utils::contains($lineItem, Data::$regex['price_dot_w']['pattern'])) {
            return true;
        } else if(Utils::contains($lineItem, Data::$regex['price_comma_w']['pattern'])) {
            return true;
        } else if($lineItem == '') {
            return true;
        } else {
            return false;
        }
    }

    private function collectPrices(array $linesStepMerge, array $pattern, array $header): array {
        $array = [];
        foreach($linesStepMerge as $lineStepMergeIndex => $lineStepMerge) {
            if($lineStepMerge) {
                foreach($lineStepMerge['content']['columns'] as $lineStepMergeItemIndex => $lineStepMergeItem) {
                    if($pattern[$lineStepMergeItemIndex] === "price" && $header[$lineStepMergeItemIndex]['property']['format'] === "price") {
                        if($contains = Utils::contains($lineStepMergeItem, Data::$regex["price_dot_w"]["pattern"])) {
                            $array[$lineStepMergeIndex][] = Utils::parsePrice($contains[0])['price'];
                        } else if($contains = Utils::contains($lineStepMergeItem, Data::$regex["price_comma_w"]["pattern"])) {
                            $array[$lineStepMergeIndex][] = Utils::parsePrice($contains[0])['price'];
                        }
                    }
                }
            }
        }
        return $array;
    }

}
