<?php

namespace App\Http\Helpers\Reader;

use App\Http\Helpers\Data;
use App\Http\Helpers\Utils;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use LanguageDetection\Language;
use App\Http\Helpers\Reader\Lines;
use App\Http\Helpers\Reader\Finder;
use App\Http\Helpers\Reader\Extractor;

class Reader {

    /** @var string $textContent Invoice text content */
    private $textContent;
    /** @var array $splittedTextContent Invoice text content splitted by line */
    private $splittedTextContent;

    /** @var array $datatablesRange Data tables starting and ending lines */
    private $datatablesRange = [];

    /**
     * Reader constructor, store the invoice text content passed as a parameter in a property
     * 
     * @param string $textContent
     * @return void
     */
    public function __construct(string $textContent) {
        $this->setTextContent($textContent);
    }

    /**
     * Main process
     * 
     * @return array
     */
    public function process(): array {

        $datatables = [];
        $linesPrices = [];

        // guess the language of the invoice
        // dd($this->textContent);
        $this->getLanguage($this->textContent);
        // split text content by line
        $this->setSplittedTextContent(Utils::splitByLine($this->getTextContent()));
        $finder = new Finder();
        // get the header line number
        $headerLineNumbers = $finder->findHeader($this->getSplittedTextContent());

        $extractor = new Extractor();
        // extract the header from text with
        $headers = $extractor->extractHeader($headerLineNumbers, $this->getSplittedTextContent());

        foreach($headers as $headerIndex => $header) {
            // extract the text lines after the header
            $lines = $extractor->extractLines($headerLineNumbers[$headerIndex], $this->getSplittedTextContent(), $header, Arr::exists($headerLineNumbers, $headerIndex + 1) ? $headerLineNumbers[$headerIndex + 1] : null);
            $linesExtractor = new Lines();
            // extract the data table lines
            $linesArray = $linesExtractor->getLines($lines, $header);
            $datatables[$headerIndex]['header'] = $header;
            $datatables[$headerIndex]['lines'] = $linesArray['lines'];
            $linesPrices[$headerIndex] = $linesArray['prices'];
            // find the data tables range
            $this->datatablesRange[] = $finder->findDataTableRange($this->splittedTextContent, $headerLineNumbers[$headerIndex], $datatables[$headerIndex]['lines']);
        }

        $parser = new Parser();
        // parse invoice text and extract important informations
        $parser->getWordGroups($this->textContent, $this->splittedTextContent, $this->datatablesRange, $linesPrices);
        
        return $datatables;
    }

    /**
     * Guess the languages of the invoice and return the best result
     * 
     * @param string $textContent
     * @return string
     */
    public function getLanguage(string $textContent) {
        $ld = new Language();
        return key($ld->detect($textContent)->bestResults()->close());
    }

    /**
     * Get the text file content
     * 
     * @return string
     */
    public function getTextContent(): string {
        return $this->textContent;
    }

    /**
     * Set the tex file content
     * 
     * @return void
     */
    public function setTextContent(string $textContent): void {
        $this->textContent = $textContent;
    }

    /**
     * Get the text content splitted by line
     * 
     * @return array
     */
    public function getSplittedTextContent(): array {
        return $this->splittedTextContent;
    }

    /**
     * Set the text content splitted by line
     * 
     * @return void
     */
    public function setSplittedTextContent(array $splittedTextContent): void {
        $this->splittedTextContent = $splittedTextContent;
    }

    /**
     * Get the data tables range
     * 
     * @return array
     */
    public function getDataTablesRange(): array {
        return $this->datatablesRange;
    }

}
