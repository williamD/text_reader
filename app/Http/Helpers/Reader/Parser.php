<?php

namespace App\Http\Helpers\Reader;

use NumberFormatter;
use App\Http\Helpers\Data;
use Whitecube\Price\Price;
use App\Http\Helpers\Utils;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Money\Parser\IntlMoneyParser;
use App\Http\Helpers\Reader\Finder;
use libphonenumber\PhoneNumberUtil;
use Money\Currencies\ISOCurrencies;

class Parser
{
    private $items = [];
    private $groups = [];
    private $result = [];
    private $invoiceCurrency = null;

    private $addresses = [];
    private $phoneNumbers = [];
    private $prices = [];
    private $emails = [];
    private $percentages = [];

    private $subsets = [];
    private $minSubset = [];
    private $subSubsets = [];

    private $subtotal = 0;
    private $discount = 0;
    private $taxValue = 0;
    private $total = 0;

    private $totalLabels = [];
    private $totalLabel = null;

    private $test = 0;

    public function __construct() {}

    /**
     * Get string positions in line
     *
     * @param array $textContent
     * @return void
     */
    public function getStringPositions(array $textContent): void
    {
        $items = [];
        foreach ($textContent as $lineIndex => $line) {
            $splittedLine = Utils::splitBySpace(4, $line);
            foreach ($splittedLine as $splittedLineItem) {
                if (!empty(trim($splittedLineItem))) {
                    $start = mb_stripos($line, trim($splittedLineItem), 0, 'UTF-8');
                    $end = Str::length(trim($splittedLineItem)) + $start - 1;
                    $items[] = [
                        'line_index' => $lineIndex,
                        'content' => trim($splittedLineItem),
                        'start' => $start,
                        'end' => $end,
                    ];
                }
            }
        }
        $this->items = $items;
    }

    /**
     * Get word groups
     *
     * @param array $textContent
     * @param array $datatableRange
     * @param array $prices
     * @return void
     */
    public function getWordGroups(string $mergedTextContent, array $textContent, array $datatablesRange, array $linesPrices): void {
        // remove datatables from text content
        $this->removeDatatable($textContent, $datatablesRange);
        // remove empty lines from text content
        $textContent = $this->removeEmptyLines($textContent);
        // get string positions in line
        $this->getStringPositions($textContent);
        $this->groups = $this->setGroups();
        $this->tagData();
        $this->removeLowerPriorityElements();
        $this->findPostalAddress();
        $this->findPhoneNumber($textContent);
        $this->findPrice($textContent);
        $this->findEmail($textContent);
        $this->findPercentage($textContent);
        
        $this->tagPrices3($linesPrices, $textContent);
        // dump($this->addresses);
        // dump($this->phoneNumbers);
        // dump($this->emails);
        // dump($this->percentages);
        // dump($this->prices);
    }

    private function findTotalPrice($textContent) {
        $finder = new Finder();
        $this->totalLabels = $finder->findTotalLabels($textContent);
        // find prices corresponding to the total labels
        $totalPrice = $this->findNearPrice($textContent);
        if(!empty($totalPrice['priceLine'])) {
            $totalPrice['total'] = $totalPrice['priceLine'];
        } else {
            if(!empty($totalPrice['priceDiagBottom'])) {
                $totalPrice['total'] = $totalPrice['priceDiagBottom']['price'];
            } else if(!empty($totalPrice['priceDiagTop'])) {
                $totalPrice['total'] = $totalPrice['priceDiagTop']['price'];
            } else {
                $totalPrice['total'] = null;
            }
        }
        return $totalPrice;
    }

    private function findNearPrice(array $splittedTextContent) {
        $possibleValues = [];

        foreach($this->totalLabels as $lineNumber => $labelsLine) {
            $priceLine = null;
            $priceDiagTop = null;
            $priceDiagBottom = null;
            $label = collect($labelsLine)->sortByDesc(function($column) {
                return mb_strlen($column);
            })->first();
            $labelPos = mb_stripos($splittedTextContent[$lineNumber], $label);
            $labelLength = mb_strlen($label);
            
            $priceLine = $this->priceLine($splittedTextContent[$lineNumber]);
            $priceDiagTop = $this->priceDiagTop($lineNumber, $splittedTextContent, $labelPos + $labelLength);
            $priceDiagBottom = $this->priceDiagBottom($lineNumber, $splittedTextContent, $labelPos + $labelLength);

            $possibleValues[] = [
                'lineNumber' => $lineNumber,
                'line' => $splittedTextContent[$lineNumber],
                'priceLine' => $priceLine,
                'priceDiagTop' => $priceDiagTop,
                'priceDiagBottom' => $priceDiagBottom,
            ];
        }

        $filteredValues = array_values(Arr::where($possibleValues, function ($item, $key) {
            if(!empty($item['priceLine'])) {
                if(!Arr::get($item['priceDiagBottom'], 'direct') && !empty($item['priceDiagTop'])) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if(!empty($item['priceDiagBottom']) && !empty($item['priceDiagTop'])) {
                    return true;
                } else {
                    return false;
                }
            }
        }));

        if(count($filteredValues) > 1) {
            $noLeftOffset = [];
            $intersection = [];
            foreach($filteredValues as $filteredValueIndex => $filteredValue) {
                if(blank(Utils::getStringLeftOffset($labelPos, $filteredValue['line']))) {
                    $noLeftOffset[$filteredValueIndex] = 1;
                }
            }
            if(count($noLeftOffset) >= 1) {
                $intersection = collect($filteredValues)->intersectByKeys($noLeftOffset)->all();
            }
        }
        
        if(isset($intersection)) {
            return last(Utils::reindexArray($intersection));
        } else {
            return last(Utils::reindexArray($filteredValues));
        }
    }

    private function priceLine(string $line) {
        if($contains = Utils::contains($line, Data::$regex['price_dot_w']['pattern'])) {
            return Utils::parsePrice($contains[0]);
        }
        if($contains = Utils::contains($line, Data::$regex['price_comma_w']['pattern'])) {
            return Utils::parsePrice($contains[0]);
        }
        return [];
    }

    /**
     * Search for prices in diagonal right before current line passed as parameter
     * 
     * @param int $lineNumber
     * @param array $splittedTextContent
     * @param int $offset
     * @return int
     */
    private function priceDiagTop(int $lineNumber, array $splittedTextContent, int $offset): array {
        for($i = $lineNumber - 1; $i >= 0; $i--) {
            $string = Utils::getStringRightOffset($offset, $splittedTextContent[$i]);
            $priceDot = Utils::contains($string, Data::$regex['price_dot_w']['pattern']);
            $priceComma = Utils::contains($string, Data::$regex['price_comma_w']['pattern']);
            if($priceDot) {
                return [
                    'lineNumber' => $i,
                    'string' => $string,
                    'price' => Utils::parsePrice($priceDot[0]),
                    'direct' => $i === $lineNumber - 1 ? true : false,
                ];
            }
            if($priceComma) {
                return [
                    'lineNumber' => $i,
                    'string' => $string,
                    'price' => Utils::parsePrice($priceComma[0]),
                    'direct' => $i === $lineNumber - 1 ? true : false,
                ];
            }
        }
        return [];
    }

    /**
     * Search for prices in diagonal right after current line passed as parameter
     * 
     * @param int $lineNumber
     * @param array $splittedTextContent
     * @param int $offset
     * @return int
     */
    private function priceDiagBottom(int $lineNumber, array $splittedTextContent, int $offset): array {
        for($i = $lineNumber + 1; $i < count($splittedTextContent); $i++) {
            $string = Utils::getStringRightOffset($offset, $splittedTextContent[$i]);
            $priceDot = Utils::contains($string, Data::$regex['price_dot_w']['pattern']);
            $priceComma = Utils::contains($string, Data::$regex['price_comma_w']['pattern']);
            if($priceDot) {
                return [
                    'lineNumber' => $i,
                    'string' => $string,
                    'price' => Utils::parsePrice($priceDot[0]),
                    'direct' => $i === $lineNumber + 1 ? true : false,
                ];
            }
            if($priceComma) {
                return [
                    'lineNumber' => $i,
                    'string' => $string,
                    'price' => Utils::parsePrice($priceComma[0]),
                    'direct' => $i === $lineNumber + 1 ? true : false,
                ];
            }
        }
        return [];
    }

    private function findTotalLabel() {
        $labels = Arr::last($this->totalLabels);
        end($this->totalLabels);
        $line = key($this->totalLabels);

        return collect($labels)->sortByDesc(function($column) {
            return mb_strlen($column);
        })->first();
    }

    /**
     * Set data groups
     *
     * @return array
     */
    public function setGroups(): array
    {
        $array = [];
        foreach ($this->items as $itemIndex => $item) {
            // items has no previous
            if (!Arr::exists($this->items, $itemIndex - 1)) {
                // add item in a new group
                $array[] = [];
                end($array);
                $array[key($array)][] = $item;
            } else {
                $found = false;
                foreach ($array as $groupIndex => $group) {
                    // set cursor on the last item of group
                    end($group);
                    // check if the current item is under the last item in a group
                    if (current($group)['line_index'] === $item['line_index'] - 1 && $this->hasIntersection($item['start'], $item['end'], current($group)['start'], current($group)['end'])) {
                        $found = true;
                        $array[$groupIndex][] = $item;
                        break;
                    }
                }
                // if item was not inserted in array, create a new one
                if (!$found) {
                    end($array);
                    $array[key($array) + 1][] = $item;
                }
            }
        }
        return $array;
    }

    /**
     * Tag data with pattern matching result
     *
     * @return void
     */
    public function tagData()
    {
        $array = [];
        $index = 0;
        $found = false;
        foreach ($this->groups as $groupIndex => $group) {
            $this->groups[$groupIndex] = [];
            foreach ($group as $itemIndex => $item) {
                foreach (Data::$regex as $regex) {
                    if (Utils::contains($item['content'], $regex['pattern'])) {
                        $this->groups[$groupIndex][] = ['id' => $index, 'item' => $item, 'regex' => $regex, 'result' => Utils::contains($item['content'], $regex['pattern'])];
                        $found = true;
                    }
                }
                if (!$found) {
                    $this->groups[$groupIndex][] = ['id' => $index, 'item' => $item, 'regex' => ['pattern' => null, 'type' => 'string', 'priority' => 0], 'result' => [$item['content']]];
                }
                $found = false;
                $index++;
            }
        }
    }

    /**
     * Find all postal addresses in text content
     *
     * @return void
     */
    public function findPostalAddress(): void
    {
        $array = [];
        $addresses = [];
        foreach ($this->groups as $wrapperIndex => $wrapper) {
            foreach ($wrapper as $groupsIndex => $groups) {
                $array[] = $groups->sortByDesc(function ($column) {
                    return mb_strlen($column['result'][0]);
                })->first();
            }
        }
        foreach ($array as $item) {
            if ($item['regex']['type'] === 'postal_code') {
                $previous = Arr::first($array, function ($value, $key) use ($item) {
                    return $value['item']['line_index'] === $item['item']['line_index'] - 1 && $this->hasIntersection($item['item']['start'], $item['item']['end'], $value['item']['start'], $value['item']['end']);
                });
                $next = Arr::first($array, function ($value, $key) use ($item) {
                    return $value['item']['line_index'] === $item['item']['line_index'] + 1 && $this->hasIntersection($item['item']['start'], $item['item']['end'], $value['item']['start'], $value['item']['end']);
                });
                $previous = empty($previous) ? null : $previous;
                $next = empty($next) ? null : $next;
                
                $previous = $this->isAddressType($previous) ? $previous : null;
                $current = $item;
                $next = $this->isAddressType($next) ? $next : null;
                $complete = trim(($previous ? $previous['item']['content'] : '') . ' ' . ($current ? $current['item']['content'] : '') . ' ' . ($next ? $next['item']['content'] : ''));
                
                $addresses[] = [
                    'complete' => $complete,
                    'previous' => $previous,
                    'current' => $current,
                    'next' => $next,
                ];
            }
        }
        $this->addresses = $addresses;
    }

    /**
     * Check if item is a part of an address
     *
     * @param array $data
     * @return bool
     */
    public function isAddressType($item): bool
    {
        if ($item) {
            if (in_array($item['regex']['type'], ['postal_code', 'street', 'name', 'string'], true)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Find all phone numbers in text content
     *
     * @param array $textContent
     *
     * @return void
     */
    public function findPhoneNumber(array $textContent): void
    {
        $array = [];
        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        foreach ($textContent as $line) {
            if (Utils::containsMoreNumbersThan($line, 6)) {
                $phoneNumberMatcherFR = $phoneNumberUtil->findNumbers($line, 'FR');
                $phoneNumberMatcherGB = $phoneNumberUtil->findNumbers($line, 'GB');
                $phoneNumberMatcherUS = $phoneNumberUtil->findNumbers($line, 'US');
                foreach ($phoneNumberMatcherFR as $phoneNumberMatch) {
                    $array[] = $phoneNumberMatch->number();
                }
                foreach ($phoneNumberMatcherGB as $phoneNumberMatch) {
                    $array[] = $phoneNumberMatch->number();
                }
                foreach ($phoneNumberMatcherUS as $phoneNumberMatch) {
                    $array[] = $phoneNumberMatch->number();
                }
            }
        }
        $this->phoneNumbers = $array;
    }

    /**
     * Find all prices in text content
     *
     * @param array $textContent
     *
     * @return void
     */
    public function findPrice(array $textContent): void
    {
        $array = [];
        $prices = [];
        foreach ($textContent as $line) {
            if(Utils::contains($line, Data::$regex['number']['pattern'])) {
                if($contains = Utils::contains($line, Data::$regex['price_dot_w']['pattern'])) {
                    foreach ($contains as $contain) {
                        $priceContain = $this->checkPrice($line, $contain);
                        if($priceContain) {
                            $prices[] = $priceContain;
                        }
                    }
                } else if($contains = Utils::contains($line, Data::$regex['price_comma_w']['pattern'])) {
                    foreach ($contains as $contain) {
                        $priceContain = $this->checkPrice($line, $contain);
                        if($priceContain) {
                            $prices[] = $priceContain;
                        }
                    }
                }/* else if($contains = Utils::contains($line, Data::$regex['price_w']['pattern'])) {
                    foreach ($contains as $contain) {
                        $priceContain = $this->checkPrice($line, $contain);
                        $prices[] = $priceContain ? $priceContain : [$contain];
                    }
                }*/
            }
        }

        foreach ($prices as $priceLine) {
            if($priceLine) {
                foreach($priceLine as $price) {
                    $parsedPrice = [
                        'default' => preg_replace("/ +/", " ", Str::of($price)->trim()),
                        'parsed' => Utils::parsePrice($price),
                    ];
                    $array[] = $parsedPrice;
                }
            }
        }
        $this->prices = $array;
    }

    /**
     * Check if price has a currency indicator
     * 
     * @param string $line
     * @param array $contains
     * 
     * @return array|null
     */
    public function checkPrice(string $line, $contain)
    {
        foreach (Data::$currencies as $currency) {
            $currencyTypes = [
                Str::lower($currency['namePlural']),
                Str::lower($currency['name']),
                Str::lower($currency['code']),
                Str::lower($currency['symbol']),
                Str::lower($currency['symbolNative']),
            ];
            foreach($currencyTypes as $currencyType) {
                if ($containsPrice = Utils::contains(Str::lower($line), "/(?<![^\s])". preg_quote(Str::lower($contain), '/') ." *". preg_quote($currencyType, '/') ."(?![^\s])/")) {
                    return $containsPrice;
                } else if($containsPrice = Utils::contains(Str::lower($line), "/(?<![^\s])". preg_quote($currencyType, '/') ." *". preg_quote(Str::lower($contain), '/') ."(?![^\s])/")) {
                    return $containsPrice;
                } else if($containsPrice = Utils::contains(Str::lower($line), "/(?<![^\s])(?:\- {0,2})". preg_quote($currencyType, '/') ." *". preg_quote(Str::lower($contain), '/') ."(?![^\s])/")) {
                    return $containsPrice;
                }
            }
            if(Arr::exists($currency, 'alternative')) {
                if ($containsPrice = Utils::contains(Str::lower($line), "/(?<![^\s])". preg_quote(Str::lower($contain), '/') ." *". preg_quote(Str::lower($currency['alternative']), '/') ."(?![^\s])/")) {
                    return $containsPrice;
                } else if($containsPrice = Utils::contains(Str::lower($line), "/(?<![^\s])". preg_quote(Str::lower($currency['alternative']), '/') ." *". preg_quote(Str::lower($contain), '/') ."(?![^\s])/")) {
                    return $containsPrice;
                } else if($containsPrice = Utils::contains(Str::lower($line), "/(?<![^\s])(?:\- {0,2})". preg_quote(Str::lower($currency['alternative']), '/') ." *". preg_quote(Str::lower($contain), '/') ."(?![^\s])/")) {
                    return $containsPrice;
                }
            }
        }
        if($containsPrice = Utils::contains(Str::lower($line), "/(?<![^\s])". preg_quote(Str::lower($contain), '/') ."(?![^\s])/")) {
            return $containsPrice;
        }
        return null;
    }

    /**
     * Find all percentages in text content
     *
     * @param array $textContent
     *
     * @return void
     */
    public function findPercentage(array $textContent): void
    {
        $array = [];
        $percentage = null;
        foreach ($textContent as $line) {
            if (Utils::contains($line, Data::$regex['percentage']['pattern'])) {
                $percentage = Utils::contains($line, Data::$regex['percentage']['pattern']);
            } elseif (Utils::contains($line, Data::$regex['percentage_invert']['pattern'])) {
                $percentage = Utils::contains($line, Data::$regex['percentage_invert']['pattern']);
            }
            if ($percentage) {
                $array[] = [
                    'default' => $percentage[0],
                    'parsed' => Utils::parsePercentage($percentage[0]),
                ];
            }
        }
        $this->percentages = $array;
    }

    public function getMinSubsetAdd(array $prices, int $sum) {
        $arr = collect($prices)->map(function($item, $key) {
            return intval(round($item['parsed']['price'] * 100));
        })->all();
        $this->getAllSubsetsAdd($arr, count($arr), $sum);
        return $this->findMinSubset($this->subsets);
    }

    public function getMinSubsetBoth(array $prices, int $sum) {
        $arr = collect($prices)->map(function($item, $key) {
            return intval(round($item['parsed']['price'] * 100));
        })->all();
        $this->getAllSubsetsBoth($arr, count($arr), $sum);
        return $this->findMinSubset($this->subsets);
    }

    public function findInvoiceAmount(array $prices, int $sum, array $minSubset) {
        $amount = null;
        $taxes = collect($this->percentages)->pluck('parsed')->merge(Data::$tva)->unique()->all();
        foreach($minSubset as $subsetItem) {
            foreach($taxes as $tax) {
                $calc = $subsetItem * (1 + $tax / 100);
                $calc = intval(round($calc));
                if($calc == $sum) {
                    $has = true;
                    // invoice total amount without tax
                    $amount = Arr::first($prices, function($value, $key) use($subsetItem) {
                        return $value['parsed']['price'] == $subsetItem / 100;
                    });
                }
            }
        }
        return $amount;
    }

    public function findInvoiceTaxValue(array $prices, int $sum, array $minSubset, array $amount) {
        $taxValue = null;
        $amountMult = $amount['parsed']['price'] * 100;
        $amountMultInt = intval(round($amountMult));

        $filteredSubset = collect($minSubset)->filter(function($item, $key) use($amountMultInt) {
            return $item != $amountMultInt;
        })->all();
        
        foreach($filteredSubset as $subsetItemIndex => $subsetItem) {
            $calc = $subsetItem + $amountMultInt;
            if($calc == $sum) {
                $taxValue = Arr::first($prices, function($value, $key) use($subsetItem) {
                    return $value['parsed']['price'] == $subsetItem / 100;
                });
            }
        }
        return $taxValue;
    }

    /**
     * 3rd try to tag prices (get the tax value, the amount, the discount value and the total amount from the invoice)
     * by 
     */
    public function tagPrices3(array $linesPrices, array $textContent) {
        $taxValue = null;
        $amount = null;
        $discount = null;
        $totalAmount = null;

        if(!$this->prices) return;
        $pricesCopy = $this->prices;
        // remove duplicate prices
        $pricesCopy = collect($pricesCopy)->unique(function($item) {
            return $item['parsed']['price'];
        })->all();
        // remove not accurate prices
        $this->removeNotAccuratePrices($pricesCopy);
        // re-index array
        $pricesCopy = Utils::reindexArray($pricesCopy);

        $totalPrice = $this->findTotalPrice($textContent);
        $totalAmount = $totalPrice['total'];

        // 
        if(Arr::exists($totalPrice, 'total') && $totalPrice['total']) {
            $this->removePricesAfter($totalPrice['total']['price'], $pricesCopy);
            $this->removePrice($totalPrice['total']['price'], $pricesCopy);
        }
        
        if($pricesCopy && $totalPrice['total']) {
            $hasDiscount = true;
            if($totalPrice['total']['price'] > 0 || $this->containsNegativePrice($pricesCopy)) {
                $hasDiscount = false;
            }

            // if invoice contains a discount value
            if(!$hasDiscount) {
                $totalPriceMult = $totalPrice['total']['price'] * 100;
                $totalPriceMultInt = intval(round($totalPriceMult));
                // get the minimal subset from prices (without subtraction)
                $minSubset = $this->getMinSubsetAdd($pricesCopy, $totalPriceMultInt);

                if($minSubset) {
                    // find the invoice amount without tax
                    $amount = $this->findInvoiceAmount($pricesCopy, $totalPriceMultInt, $minSubset);
                }
                if($amount) {
                    // find the invoice tax value
                    $taxValue = $this->findInvoiceTaxValue($pricesCopy, $totalPriceMultInt, $minSubset, $amount);
                }
                
                if(!$amount || !$taxValue) {
                    $hasDiscount = true;
                }
            }
            if($hasDiscount || $this->hasNoSubset($this->subsets)) {
                $totalPriceMult = $totalPrice['total']['price'] * 100;
                $totalPriceMultInt = intval(round($totalPriceMult));
                // get the minimal subset from prices (with subtraction)
                $minSubset = $this->getMinSubsetBoth($pricesCopy, $totalPriceMultInt);
                // dump($pricesCopy);
                if($minSubset) {
                    if(array_sum($minSubset) > $totalPriceMultInt) {
                        $subsetCollection = collect($minSubset);
                        //dump($subsetCollection);
                        // foreach($minSubset as $subsetItemIndex => $subsetItem) {
                        //     $filteredSubset = $subsetCollection->except([$subsetItemIndex]);
                        //     $filteredSubset->merge(1 + $subsetItem);
                        //     dump([$sum, $filteredSubset]);
                        // }

                        // $this->computeSubRecursive($subsetCollection, $totalPriceMultInt);
                        // $this->computeSubRecursive(collect([5, 10, 12, 9, 7]), 11);
                        $this->compute($subsetCollection, 5);
                    }
                }
                // if no subset, sum must be equal to 0 => need to find a solution
                if($this->hasNoSubset($this->subsets)) {
                    
                }
            }
        }
    }

    public function compute(\Illuminate\Support\Collection $subsetCollection, int $sum) {
        
    }

    /**
     * @deprecated
     */
    public function tagPrices2($linesPrices) {
        
        $taxValue = null;
        $amount = null;
        $totalAmount = null;

        if(!$this->prices) return;
        $pricesCopy = $this->prices;
        $linesMaxPrices = $this->getLinesMaxPrices($linesPrices);
        $linesPricesSum = array_sum($linesMaxPrices);
        $invoiceMaxPrice = $this->findMaxPrice($pricesCopy);
        
        //dump([$linesPricesSum, $invoiceMaxPrice['parsed']['price'], Arr::pluck($pricesCopy, 'parsed.price')]);
        /*$totalPrice = Arr::last($pricesCopy);
        end($pricesCopy);
        unset($pricesCopy[key($pricesCopy)]);
        $arr = [];
        foreach($pricesCopy as $priceCopy) {
            $price = $priceCopy['parsed']['price'] * 100;
            $arr[] = intval(round($price));
        }
        $n = count($arr);
        $totalPriceInt = $totalPrice['parsed']['price'] * 100;
        $sum = intval(round($totalPriceInt));
        $this->getAllSubsets($arr, $n, $sum);*/
        //dump([$sum, $arr, $this->subsets]);


        //dump([$totalPrice['parsed']['price'], Arr::pluck($pricesCopy, 'parsed.price')]);
        /*if($linesPricesSum < $invoiceMaxPrice) {
            $this->removeMaxPrice($invoiceMaxPrice, $pricesCopy);
            //dump([$linesPricesSum, $this->findMaxPrice($pricesCopy)['parsed']['price'], Arr::pluck($pricesCopy, 'parsed.price')]);
            if(array_sum(Arr::pluck($pricesCopy, 'parsed.price'))) {

            }
        }*/

        
        //dump($pricesCopy);
        $this->removeNotAccuratePrices($pricesCopy);

        // $totalPrice = Arr::last($pricesCopy);
        // $totalAmount = $totalPrice;

        $this->removeMaxPrice($totalPrice, $pricesCopy);
        // dump([$totalPrice['parsed']['price'], Arr::pluck($pricesCopy, 'parsed.price')]);
        $hasDiscount = true;
        if($totalPrice['parsed']['price'] > 0 || $this->containsNegativePrice($pricesCopy)) {
            $hasDiscount = false;
        }
        if(!$hasDiscount) {
            //dump([$linesPricesSum, $totalPrice['parsed']['price'], Arr::pluck($pricesCopy, 'parsed.price')]);
            $arr = [];
            foreach($pricesCopy as $priceCopy) {
                $price = $priceCopy['parsed']['price'] * 100;
                $arr[] = intval(round($price));
            }
            $totalPriceMult = $totalPrice['parsed']['price'] * 100;
            $sum = intval(round($totalPriceMult));
            $this->getAllSubsetsAdd($arr, count($arr), $sum);
            $minSubset = $this->findMinSubset($this->subsets);
            $taxes = array_unique(array_merge(Arr::pluck($this->percentages, 'parsed'), Data::$tva));
            $has = false;
            
            //dump([Arr::pluck($pricesCopy, 'parsed.price'), $this->subsets]);
            if($minSubset) {
                foreach($minSubset as $subsetItem) {
                    foreach($taxes as $tax) {
                        $calc = $subsetItem * (1 + $tax / 100);
                        $calc = intval(round($calc));
                        if($calc == $sum) {
                            $has = true;
                            // invoice total amount without tax
                            $amount = Arr::first($pricesCopy, function($value, $key) use($subsetItem) {
                                return $value['parsed']['price'] == $subsetItem / 100;
                            });
                        }
                    }
                }
                if($amount) {
                    $amountMult = $amount['parsed']['price'] * 100;
                    $amountInt = intval(round($amountMult));
                    foreach($minSubset as $subsetItemIndex => $subsetItem) {
                        if($subsetItem == $amountInt) {
                            unset($minSubset[$subsetItemIndex]);
                        }
                    }
                    //dump($minSubset);
                    foreach($minSubset as $subsetItemIndex => $subsetItem) {
                        $calc = $subsetItem + $amountInt;
                        if($calc == $sum) {
                            $taxValue = Arr::first($pricesCopy, function($value, $key) use($subsetItem) {
                                return $value['parsed']['price'] == $subsetItem / 100;
                            });
                        }
                    }
                }
                if(!$has) {
                    //dump(["nok", $calc, $sum, $minSubset]);
                }
                
            }
            //dump([$amount, $taxValue, $totalAmount]);
            /*if($linesPricesSum !== $totalPrice) {

            }*/
        }
        if($hasDiscount || $this->hasNoSubset($this->subsets)) {
            //dump($pricesCopy);
            $arr = [];
            foreach($pricesCopy as $priceCopy) {
                $price = $priceCopy['parsed']['price'] * 100;
                $arr[] = intval(round($price));
            }
            $totalPriceMult = $totalPrice['parsed']['price'] * 100;
            $sum = intval(round($totalPriceMult));
            $this->getAllSubsetsBoth($arr, count($arr), $sum);
            //dump([Arr::pluck($pricesCopy, 'parsed.price'), $this->subsets]);
            $minSubset = $this->findMinSubset($this->subsets);
            if($minSubset) {
                if(array_sum($minSubset) > $sum) {
                    $subsetCollection = collect($minSubset);
                    //dump($subsetCollection);
                    // foreach($minSubset as $subsetItemIndex => $subsetItem) {
                    //     $filteredSubset = $subsetCollection->except([$subsetItemIndex]);
                    //     $filteredSubset->merge(1 + $subsetItem);
                    //     dump([$sum, $filteredSubset]);
                    // }

                    $this->computeSubRecursive($subsetCollection, $sum);
                    dump($this->subSubsets);
                }
            }
            // if no subset, sum must be equal to 0 => need to find a solution
            if($this->hasNoSubset($this->subsets)) {
                
            }
            
        }
    }

    /**
     * @todo find a way to make it faster with more verifications
     */
    public function computeSubRecursive(\Illuminate\Support\Collection $subsetCollection, int $sum) {
        $this->test++;
        if($subsetCollection->sum() == $sum) {
            $this->subSubsets[] = $subsetCollection->all();
            return;
        }
        $subsetCollection->each(function($item, $key) use($subsetCollection, $sum) {
            if($item > 0) {
                $filteredSubset = $subsetCollection->except($key);
                $filteredSubset = $filteredSubset->merge(0 - $item);
                $this->computeSubRecursive($filteredSubset, $sum);
            }
        });
    }

    

    /*public function alreadyInArray(array $element, array $arrays): bool {
        foreach($arrays as $array) {
            $inArray = true;
            foreach($element as $item) {
                if(!in_array($item, $array)) {
                    $inArray = false;
                    break;
                }
            }
            if($inArray) {
                return true;
            }
        }
        return false;
    }*/

    /**
     * Check if invoice has no prices subsets
     * 
     * @param array $subsets
     * @return boolean
     */
    public function hasNoSubset(array $subsets): bool {
        if($subsets && !empty($subsets)) {
            return false;
        }
        $allSubsetsEmpty = true;
        foreach($subsets as $subset) {
            if(!empty($subset) && $subset) {
                $allSubsetsEmpty = false;
            }
        }
        return $allSubsetsEmpty;
    }

    public function removeNotAccuratePrices(array &$prices) {
        $maxPrice = $this->findMaxPrice($prices);
        $sumPrices = array_sum(Arr::pluck($prices, 'parsed.price')) - $maxPrice['parsed']['price'];
        if($maxPrice['parsed']['price'] > $sumPrices) {
            $this->removeMaxPrice($maxPrice, $prices);
            $this->removeNotAccuratePrices($prices);
        }
    }

    public function containsNegativePrice(array $prices): bool {
        foreach($prices as $price) {
            if($price['parsed']['price'] < 0) {
                return true;
            }
        }
        return false;
    }

    public function removeMaxPrice(array $maxPrice, array &$prices): void {
        foreach($prices as $priceIndex => $price) {
            if($price['parsed']['price'] === $maxPrice['parsed']['price']) {
                unset($prices[$priceIndex]);
            }
        }
    }

    public function removePrice(float $targetPrice, array &$prices): void {
        foreach($prices as $priceIndex => $price) {
            if($price['parsed']['price'] === $targetPrice) {
                unset($prices[$priceIndex]);
            }
        }
    }

    public function removePricesAfter(float $targetPrice, array &$prices) {
        $remove = false;
        foreach($prices as $priceIndex => $price) {
            if($remove) {
                unset($prices[$priceIndex]);
            }
            if($price['parsed']['price'] === $targetPrice) {
                $remove = true;
            }
        }
    }

    public function getLinesMaxPrices(array $linesPrices) {
        $maxPrices = [];
        foreach($linesPrices as $tablePrices) {
            foreach($tablePrices as $linePrices) {
                $maxPrices[] = max($linePrices);
            }
        }
        return $maxPrices;
    }

    /**
     * @deprecated
     */
    public function tagPrices() {
        $pricesCopy = $this->prices;
        $maxPrice = $this->findMaxPrice($pricesCopy);
        foreach($pricesCopy as $priceIndex => $price) {
            if($price['parsed']['price'] == $maxPrice['parsed']['price']) {
                unset($pricesCopy[$priceIndex]);
            }
        }

        $arr = [];
        foreach($pricesCopy as $priceCopy) {
            $arr[] = intval(round($priceCopy['parsed']['price'] * 100));
        }
        $arr = array_values(array_unique($arr));
        $sum = intval(round($maxPrice['parsed']['price'] * 100));
        $n = count($arr);
        var_dump($arr);
        $this->getAllSubsets($arr, $n, $sum);
        
        while(empty($this->subsets)) {
            $maxPrice = $this->findMaxPrice($pricesCopy);
            if($maxPrice) {
                foreach($pricesCopy as $priceIndex => $price) {
                    if($price['parsed']['price'] == $maxPrice['parsed']['price']) {
                        unset($pricesCopy[$priceIndex]);
                    }
                }
                $arr = [];
                foreach($pricesCopy as $priceCopy) {
                    $arr[] = intval(round($priceCopy['parsed']['price'] * 100));
                }
                $arr = array_values(array_unique($arr));
                $sum = intval(round($maxPrice['parsed']['price'] * 100));
                $n = count($arr);
                var_dump($arr);
                $this->getAllSubsets($arr, $n, $sum);
            } else {
                break;
            }
        }
        
        if(empty($this->subsets)) {
            if(!$this->hasNegativeNumbers(Arr::pluck($this->prices, "parsed.price"))) {
                /*$indexToInvert = 0;
                while(empty($this->subsets)) {
                    $arrN = [];
                    foreach($this->prices as $price) {
                        $arrN[] = round($price['parsed']['price'] * 100);
                    }
                    $indexToInvert++;
                }*/
                //$this->tagPricesNegative($this->prices);
            }
        }
        //var_dump($this->subsets);
        // var_dump($maxPrice);
        //$this->findTaxValue($this->findMinSubset($this->subsets), $maxPrice);
        //$this->findDiscount($this->findMinSubset($this->subsets));
    }

    public function findMaxPrice($prices) {
        $pricesCopy = $prices;
        $max = 0;
        $result = 0;
        // get max price value
        foreach($pricesCopy as $price) {
            if($price['parsed']['price'] > $max) {
                $max = $price['parsed']['price'];
            }
        }
        // get prices with max value
        $maxPrices = Arr::where($pricesCopy, function ($value, $key) use($max) {
            return $value['parsed']['price'] === $max;
        });
        $maxPrice = head($maxPrices);
        return $maxPrice;
    }

    public function findMinSubset(array $subsets) {
        $min = null;
        foreach($subsets as $subsetIndex => $subset) {
            if($subsetIndex === 0) {
                $min = $subset;
            }
            if(count($subset) < count($min)) {
                $min = $subset;
            }
        }
        return $min;
    }

    public function findMaxSubset(array $subsets) {
        $max = null;
        foreach($subsets as $subsetIndex => $subset) {
            if($subsetIndex === 0) {
                $max = $subset;
            }
            if(count($subset) > count($max)) {
                $max = $subset;
            }
        }
        return $max;
    }

    public function findDiscount(array $subset) {
        $discounts = Arr::where($subset, function ($value, $key) {
            return $value < 0;
        });
        var_dump($discounts);
        var_dump(array_sum($discounts));
    }

    public function findTaxValue(array $subset, array $maxPrice) {
        $taxValue = null;
        foreach($subset as $value1) {
            $calc1 = round($value1 * 1.2);
            var_dump([$calc1, $maxPrice['parsed']['price']]);
            if((float) ($calc1 / 100) === (float) $maxPrice['parsed']['price']) {
                var_dump($calc1);
            }
        }
    }

    public function getAllSubsetsRecAdd(array $arr, int $n, array $v, int $sum) {
        if ($sum == 0) {
            $this->subsets[] = $v;
            return;
        }
        if ($n == 0) {
            return;
        }
        $this->getAllSubsetsRecAdd($arr, $n - 1, $v, $sum);
        $v[] = $arr[$n - 1];
        $this->getAllSubsetsRecAdd($arr, $n - 1, $v, $sum - $arr[$n - 1]);
    }
    
    public function getAllSubsetsAdd(array $arr, int $n, int $sum) {
        $v = [];
        $this->getAllSubsetsRecAdd($arr, $n, $v, $sum);
    }

    public function getAllSubsetsRecSub(array $arr, int $n, array $v, int $sum) {
        if ($sum == 0) {
            $this->subsets[] = $v;
            return;
        }
        if ($n == 0) {
            return;
        }
        $this->getAllSubsetsRecSub($arr, $n - 1, $v, $sum);
        $v[] = $arr[$n - 1];
        $this->getAllSubsetsRecSub($arr, $n - 1, $v, $sum + $arr[$n - 1]);
    }
    
    public function getAllSubsetsSub(array $arr, int $n, int $sum) {
        $v = [];
        $this->getAllSubsetsRecSub($arr, $n, $v, $sum);
    }

    public function getAllSubsetsRecBoth(array $arr, int $n, array $v, int $sum) {
        if ($sum == 0) {
            $this->subsets[] = $v;
            return;
        }
        if ($n == 0) {
            return;
        }
        $this->getAllSubsetsRecBoth($arr, $n - 1, $v, $sum);
        $v[] = $arr[$n - 1];
        $this->getAllSubsetsRecBoth($arr, $n - 1, $v, $sum - $arr[$n - 1]) || $this->getAllSubsetsRecBoth($arr, $n - 1, $v, $sum + $arr[$n - 1]);
    }
    
    public function getAllSubsetsBoth(array $arr, int $n, int $sum) {
        $v = [];
        $this->getAllSubsetsRecBoth($arr, $n, $v, $sum);
    }

    public function hasNegativeNumbers(array $prices) {
        foreach($prices as $value) {
            if($value < 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Find all emails in text content
     *
     * @param array $textContent
     *
     * @return void
     */
    public function findEmail(array $textContent): void
    {
        $array = [];
        foreach ($textContent as $line) {
            $contains = Utils::contains($line, Data::$regex['email']['pattern']);
            if ($contains) {
                $array[] = $contains[0];
            }
        }
        $this->emails = $array;
    }

    /**
     * Remove elements having lower priority in regex matching
     *
     * @return void
     */
    public function removeLowerPriorityElements(): void
    {
        $array = [];
        foreach ($this->groups as $wrapper) {
            $array[] = collect($wrapper)->groupBy('id')->map(function ($item, $key) {
                return $item->where('regex.priority', $item->max('regex.priority'));
            });
        }
        $this->groups = $array;
    }

    /**
     * Remove data table from text content
     *
     * @param array $textContent
     *      text content splitted by lines
     * @param array $tableRange
     *      data table start and end lines value
     * @return void
     */
    public function removeDatatable(array &$textContent, array $tablesRange): void
    {
        // not using array_splice to preserve keys and allow to
        // remove multiple datatables from one invoice
        $process = false;
        foreach($tablesRange as $tableRange) {
            foreach($textContent as $lineIndex => $line) {
                if($lineIndex === $tableRange['start']) {
                    $process = true;
                }
                if($process) {
                    unset($textContent[$lineIndex]);
                }
                if($lineIndex === $tableRange['end']) {
                    $process = false;
                }
            }
        }
    }

    /**
     * Remove empty lines from text content
     *
     * @param array $textContent
     *      text content splitted by lines
     * @return array
     */
    public function removeEmptyLines(array $textContent): array
    {
        $array = [];
        foreach ($textContent as $lineIndex => $line) {
            if (!$this->isEmptyLine($line)) {
                $array[] = $line;
            }
        }
        return $array;
    }

    /**
     * Check if the line is empty
     * 
     * @param array $line
     * @return bool
     */
    private function isEmptyLine(string $line): bool
    {
        $empty = true;
        if (!empty(trim($line))) {
            $empty = false;
        }
        return $empty;
    }

    /**
     * Check if 2 values intersect
     *
     * @param int $startValue
     * @param int $endValue
     * @param int $start
     * @param int $end
     * @return int
     */
    public function hasIntersection(int $startValue, int $endValue, int $start, int $end): bool
    {
        $rangeValue = range($startValue, $endValue);
        $range = range($start, $end);
        foreach ($rangeValue as $rangeValueItem) {
            // check if current item is contained in range
            if (in_array($rangeValueItem, $range)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Merge array values contained between 2 keys in a string
     *
     * @param array $array
     * @param int $start
     * @param int $end
     * @return string
     */
    protected function joinBetweenKeys(array $array, int $start, int $end): string
    {
        $string = '';
        foreach ($array as $key => $item) {
            if ($key === $end) {
                $string .= $item;
                return $string;
            }
            if ($key >= $start) {
                $string .= $item;
            }
        }
        return $string;
    }

    /**
     * Get addresses
     *
     * @return array
     */
    public function getAddresses(): array
    {
        return $this->addresses;
    }

    /**
     * Set addresses
     *
     * @param array $addresses
     *
     * @return void
     */
    public function setAddresses(array $addresses): void
    {
        $this->addresses = $addresses;
    }

    /**
     * Get phone numbers
     *
     * @return array
     */
    public function getPhoneNumbers(): array
    {
        return $this->phoneNumbers;
    }

    /**
     * Set phone numbers
     *
     * @param array $phoneNumbers
     *
     * @return void
     */
    public function setPhoneNumbers(array $phoneNumbers): void
    {
        $this->phoneNumbers = $phoneNumbers;
    }

    /**
     * Get prices
     *
     * @return array
     */
    public function getPrices(): array
    {
        return $this->prices;
    }

    /**
     * Set prices
     *
     * @param array $prices
     *
     * @return void
     */
    public function setPrices(array $prices): void
    {
        $this->prices = $prices;
    }
}
