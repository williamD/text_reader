<?php

namespace App\Http\Controllers;

use App\Http\Helpers\NLP\Classifier;

class NLPController extends Controller {
    
    public function index() {
        $classifier = new Classifier;
        $classifier->process();
        return view('nlp', []);
    }
}
