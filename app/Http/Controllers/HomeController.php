<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Utils;
use Illuminate\Support\Str;
use App\Jobs\ProcessInvoice;
use Illuminate\Http\Request;
use App\Http\Helpers\Extract;
use App\Http\Helpers\Performance;
use App\Http\Helpers\Reader\Finder;
use App\Http\Helpers\Reader\Parser;
use App\Http\Helpers\Reader\Reader;
use App\Jobs\ProcessInvoiceLoading;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Imtigger\LaravelJobStatus\TrackableJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class HomeController extends Controller
{
    use DispatchesJobs;
    
    public function index() {
        $performance = new Performance;
        $performance->start();

        $content = null;
        $array = [];
        $results = [];

        $files = Storage::disk('seeder')->files();

        foreach($files as $file) {
            // $file = "ViewCenterBill_1.txt";
            $content = Storage::disk('seeder')->get($file);
            $reader = new Reader($content);
            $results[] = $reader->process();
            end($results);
            $results[key($results)]['filename'] = $file;
        }
        //$results = [];
        //ProcessInvoice::dispatchAfterResponse('seeder');
        
        /*Artisan::call('pdf:convert', [
            'filePath' => Storage::disk('pdf')->path('Invoice.pdf'),
        ]);*/
        
        $performance->stop();
        $performanceData = $performance->getResult();
        return view('home', [
            'results' => $results,
            'bannerContent' => $performanceData,
        ]);
    }
}
