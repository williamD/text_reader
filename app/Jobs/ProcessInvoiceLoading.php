<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Http\Helpers\Reader\Reader;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Imtigger\LaravelJobStatus\Trackable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ProcessInvoiceLoading implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Trackable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->prepareStatus();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $files = Storage::disk('seeder')->files();
        $this->setProgressMax(count($files));

        foreach($files as $fileIndex => $file) {
            $content = Storage::disk('seeder')->get($file);
            $reader = new Reader($content);
            $results[] = $reader->process();
            end($results);
            $results[key($results)]['filename'] = $file;
            $this->setProgressNow($fileIndex);
        }
    }
}
