<?php

namespace App\Jobs;

use App\Models\Invoice;
use Illuminate\Support\Arr;
use Illuminate\Bus\Queueable;
use App\Http\Helpers\Reader\Reader;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ProcessInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The invoice instance
     * 
     * @var \App\Models\Invoice
     */
    protected $diskName;

    /**
     * Create a new job instance.
     *
     * @param App\Models\Invoice $invoice
     * @return void
     */
    public function __construct(string $diskName)
    {
        $this->diskName = $diskName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $files = Storage::disk($this->diskName)->files();

        foreach($files as $file) {
            $content = Storage::disk($this->diskName)->get($file);
            $reader = new Reader($content);
            $result = $reader->process();

            $invoice = Invoice::create([
                'path' => $file,
            ]);

            foreach($result['lines'] as $lineIndex => $line) {
                $prepared = [];
                foreach($line as $lineItemIndex => $lineItem) {
                    $prepared[$result['header'][$lineItemIndex]['property']['name']][] = $lineItem;
                }
                if(count($prepared['price']) > 1) {
                    if($prepared['price'][0] > $prepared['price'][1]) {
                        $prepared['price_ttc'][0] = $prepared['price'][0];
                        $prepared['price'][0] = $prepared['price'][1];
                    } else if($prepared['price'][0] < $prepared['price'][1]) {
                        $prepared['price_ttc'][0] = $prepared['price'][1];
                        $prepared['price'][0] = $prepared['price'][0];
                    } else {
                        $prepared['price_ttc'][0] = $prepared['price'][1];
                        $prepared['price'][0] = $prepared['price'][0];
                    }
                }
                $invoice->lines()->create([
                    'reference' => Arr::exists($prepared, 'reference') ? $prepared['reference'][0] ? $prepared['reference'][0] : null : null,
                    'date' => Arr::exists($prepared, 'date') ? $prepared['date'][0] ? $prepared['date'][0] : null : null,
                    'object' => Arr::exists($prepared, 'object') ? $prepared['object'][0] ? $prepared['object'][0] : null : null,
                    'quantity' => Arr::exists($prepared, 'quantity') ? $prepared['quantity'][0] ? $prepared['quantity'][0] : null : null,
                    'price_ht' => Arr::exists($prepared, 'price') ? $prepared['price'][0] ? $prepared['price'][0] : null : null,
                    'price_ttc' => Arr::exists($prepared, 'price_ttc') ? $prepared['price_ttc'][0] ? $prepared['price_ttc'][0] : null : null,
                    'amount' => Arr::exists($prepared, 'amount') ? $prepared['amount'][0] ? $prepared['amount'][0] : null : null,
                    'tax_amount' => Arr::exists($prepared, 'tax_amount') ? $prepared['tax_amount'][0] ? $prepared['tax_amount'][0] : null : null,
                ]);
            }
        }

    }
}
