<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class PDFConvert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pdf:convert {filePath}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert a PDF file to text';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $process = new Process(['pdftotext', '-table', '-nopgbrk', '-enc', 'UTF-8', $this->argument('filePath')]);
        $process->run();
    }
}
