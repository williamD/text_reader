# Invoice parser

## Introduction

L'objectif de cet outil est l'extraction des informations importantes d'une facture.

Pour le moment, cet outil permet l'extraction des tableaux de données contenant les articles facturés ainsi que les différents éléments importants d'une facture comme les adresses postales, les adresses email, les numéros de téléphones, les pourcentages et les prix.

Les dates, numéros de SIRET et autres informations facilement identifiables (code IBAN, code BIC/Swift, numéro de TVA) ne sont pas extraits mais des expressions régulières sont prévues dans le fichier `Data.php`.

## Installation

Cet outil est développé avec le framework Laravel.

Cloner le dépôt

```bash
git clone https://gitlab.com/williamD/text_reader.git
```

Se rendre dans le dossier du dépôt

```bash
cd text_reader
```

Installer les dépendances en utilisant composer

```bash
composer install
```

Copier le fichier `.env.example` et changer la configuration dans le fichier `.env`

```bash
cp .env.example .env
```

Générer une nouvelle clé pour l'application

```bash
php artisan key:generate
```

Démarrer le serveur de développement local

```bash
php artisan serve
```

Installer des dépendances npm

```bash
npm install
npm run dev
```

## Préparation des données

Cet outil extrait les informations de fichiers au format texte. Il est donc nécessaire de convertir les factures PDF en fichiers texte.

Pour cela, il est nécessaire d'utiliser la commande [pdftotext](https://www.xpdfreader.com/pdftotext-man.html) de l'outil XpdfReader avec les options suivantes.

```bash
./pdftotext -table -nodiag -nopgbrk -enc UTF-8 invoice.pdf
```

Cette commande va générer un fichier texte à partir du fichier pdf en respectant l'agencement des tableaux présent dans celle-ci.
Cela est nécessaire au bon fonctionnement de l'outil.

Un build modifié de `pdftotext` est disponible à la racine du projet pour permettre l'extraction des fichiers PDF protégés.

## Limitations

La commande `pdftotext` ne fonctionne seulement si le fichier PDF contient du texte brut, il ne s'agit pas d'un outil d'OCR et donc les images ne seront pas prises en compte.

La langue principale de la facture est détectée, cependant, l'outil ne fonctionne pour le moment qu'avec des factures en français.

## Structure du projet

```bash
app
┗ Http
  ┗ Helpers
    ┣ NLP
    ┃ ┗ Classifier.php
    ┣ Reader
    ┃ ┣ Extractor.php  # Extrait l'entête et les lignes
    ┃ ┣ Finder.php     # Trouve l'en-tête du tableau dans la facture
    ┃ ┣ Lines.php      # Formatte les lignes
    ┃ ┣ Parser.php     # Analyse la facture
    ┃ ┗ Reader.php
    ┣ Data.php         # Données utiles pour l'extraction des infos
    ┣ Performance.php  # Mesure des performances
    ┗ Utils.php        # Fonctions utilitaires
```

Jeu de données de factures au format texte dans le répertoire `storage/app/seeder/`

## Fonctionnement

### 1. Extraction de l'entête du tableau

Pour extraire le tableau des articles facturés il est tout d'abord nécessaire d'extraire son entête.
Étant donné que `pdftotext` conserve l'agencement du tableau, il sera alors plus simple d'extraire les colonnes et les lignes.

Exemple d'entête trouvable dans une facture:

```
DESCRIPTION	    DATE	QTÉ	    PRIX UNITAIRE	TVA	    MONTANT
```

Pour extraire l'entête, chaque terme à rechercher est associé à plusieurs propriétés comme son nom, un dictionnaire de synonymes à rechercher, un type et un format pour valider la recherche et une valeur booléenne pour indiquer si cette valeur peut se retrouvée sur plusieurs lignes.

Exemple pour la quantité:

```php
// Data.php
'quantity' => [
    'name' => 'quantity',
    'terms' => [                // Termes à rechercher dans le corps de la facture
        "quantité",
        "quantite",
        "nombre",
        "qté",
        "qte",
        "qty",
        "nb",
    ],
    'types' => ['int'],
    'format' => 'number',       // Format de validation
    'multiline' => false,       // Peut contenir plusieurs lignes ou non
],
```

Les termes à rechercher sont doivent êtres triés par ordre décroissant de longueur pour être sûr d'extraire la bonne chaîne de caractères (ex: ne pas extraire _prix total_ au lieu de _prix total ttc_).

Les lignes contenants le plus d'éléments sont identifiées en tant qu'entête pour la suite du processus.

### 2. Extraction des lignes du tableau

Les lignes sont extraites et formattées pour correspondre à l'entête (fichier `Lines.php`).
Plusieurs vérifications sont effectuées pour être sûr que les éléments présents dans le cellules sont bien conformes au type et format attendus par la colonne de l'entête.

`pdftotext` n'étant pas fiable à 100% dans le respect de l'agencement des tableaux, avec la méthode d'extraction utilisée, il se peut que certains champs faiblement typés (ex: object (string)) se retrouvent dupliqués dans la mauvaise colonne.

Exemple simple, une seule ligne, pas d'erreurs
|DESCRIPTION| DATE| QTÉ| PRIX UNITAIRE| TVA| MONTANT|
|---|---|---|---|---|---|
|T-Shirt| 01/03/2021| 1,00| 59.99| 20,0 %| 59.99|

Exemple sur deux lignes avec duplication de colonnes

| TVA           | RÉFÉRENCE           | QUANTITÉ | LIBELLÉ                                                              | PRIX NET HT | PRIX NET TTC | MONTANT NET HT |
| ------------- | ------------------- | -------- | -------------------------------------------------------------------- | ----------- | ------------ | -------------- |
| I `004549645` | `004549645` 9197117 | 1        | Console Nintendo Switch Edition limitée / Nintendo / Nintendo France | 266.16      | 319.39       | 266.16         |
|               |                     |          | Eco-Participation DEEE                                               | 0.50        | 0.60         | 0.50           |

### 3. Extraction des autres éléments

-   Numéros de téléphone

    Pour extraire les numéros de téléphone, la bibliothèque [giggsey/libphonenumber-for-php](https://github.com/giggsey/libphonenumber-for-php) est utilisée.

-   Autres

    Pour les prix, les adresses postales, les adresses email et les pourcentages, des expressions régulières sont utilisées.

-   Étiquetage des prix (Partie non finalisée)

    Objectif: trouver à quoi correspond un prix (total, sous-total, remise...).

    Fichier `Parser.php`
