<?php

namespace Tests\Unit;

use App\Http\Helpers\Utils;
use PHPUnit\Framework\TestCase;

class UtilsTest extends TestCase
{

    public function containsPriceProvider() {
        return [
            ["14000€", true],
            ["14000 €", true],
            ["14000  €", true],
            ["14000   €", false],

            ["14000 €", true],
            ["14000 $", true],
            ["14000 £", true],
            ["14000 EUR", true],
            ["14000 USD", true],
            ["14000 GBP", true],

            ["14 000 €", true],
            ["14'000 €", true],
            ["14,000 €", false],
            ["14.000 €", true],

            ["14 5000 €", false],
            ["14'5000 €", false],
            ["14,5000 €", false],
            ["14.5000 €", false],

            ["28000.532 €", false],
            ["28000,532 €", false],
            ["28000.53 €", false],
            ["28000,53 €", false],
            ["28000.5 €", false],
            ["28000,5 €", false],
            ["28000. €", false],
            ["28000, €", false],

            ["€14000", false],
            ["€ 14000", false],
            ["€  14000", false],
            ["€   14000", false],

            ["€ 14000", false],
            ["$ 14000", false],
            ["£ 14000", false],
            ["EUR 14000", false],
            ["USD 14000", false],
            ["GBP 14000", false],

            ["€ 14 000", false],
            ["€ 14'000", false],
            ["€ 14,000", false],
            ["€ 14.000", false],

            ["€ 14 5000", false],
            ["€ 14'5000", false],
            ["€ 14,5000", false],
            ["€ 14.5000", false],

            ["€ 28000.532", false],
            ["€ 28000,532", false],
            ["€ 28000.53", false],
            ["€ 28000,53", false],
            ["€ 28000.5", false],
            ["€ 28000,5", false],
            ["€ 28000.", false],
            ["€ 28000,", false],
        ];
    }

    /**
     * @dataProvider containsPriceProvider
     * @return void
     */
    public function testContainsPrice($value, $expected) {
        $this->assertSame($expected, !empty(Utils::containsPrice($value)));
    }

    public function containsPriceInvertProvider() {
        return [
            ["14000€", false],
            ["14000 €", false],
            ["14000  €", false],
            ["14000   €", false],

            ["14000 €", false],
            ["14000 $", false],
            ["14000 £", false],
            ["14000 EUR", false],
            ["14000 USD", false],
            ["14000 GBP", false],

            ["14 000 €", false],
            ["14'000 €", false],
            ["14,000 €", false],
            ["14.000 €", false],

            ["14 5000 €", false],
            ["14'5000 €", false],
            ["14,5000 €", false],
            ["14.5000 €", false],

            ["28000.532 €", false],
            ["28000,532 €", false],
            ["28000.53 €", false],
            ["28000,53 €", false],
            ["28000.5 €", false],
            ["28000,5 €", false],
            ["28000. €", false],
            ["28000, €", false],

            ["€14000", true],
            ["€ 14000", true],
            ["€  14000", true],
            ["€   14000", false],

            ["€ 14000", true],
            ["$ 14000", true],
            ["£ 14000", true],
            ["EUR 14000", true],
            ["USD 14000", true],
            ["GBP 14000", true],

            ["€ 14 000", true],
            ["€ 14'000", true],
            ["€ 14,000", false],
            ["€ 14.000", true],

            ["€ 14 5000", false],
            ["€ 14'5000", false],
            ["€ 14,5000", false],
            ["€ 14.5000", false],

            ["€ 28000.532", false],
            ["€ 28000,532", false],
            ["€ 28000.53", false],
            ["€ 28000,53", false],
            ["€ 28000.5", false],
            ["€ 28000,5", false],
            ["€ 28000.", false],
            ["€ 28000,", false],
        ];
    }

    /**
     * @dataProvider containsPriceInvertProvider
     * @return void
     */
    public function testContainsPriceInvert($value, $expected) {
        $this->assertSame($expected, !empty(Utils::containsPriceInvert($value)));
    }

    public function containsPriceDotProvider() {
        return [
            ["14000€", false],
            ["14000 €", false],
            ["14000  €", false],
            ["14000   €", false],

            ["14000 €", false],
            ["14000 $", false],
            ["14000 £", false],
            ["14000 EUR", false],
            ["14000 USD", false],
            ["14000 GBP", false],

            ["14 000 €", false],
            ["14'000 €", false],
            ["14,000 €", false],
            ["14.000 €", false],

            ["14 5000 €", false],
            ["14'5000 €", false],
            ["14,5000 €", false],
            ["14.5000 €", false],

            ["28000.532 €", false],
            ["28000,532 €", false],
            ["28000.53 €", true],
            ["28000,53 €", false],
            ["28000.5 €", true],
            ["28000,5 €", false],
            ["28000. €", false],
            ["28000, €", false],

            ["€14000", false],
            ["€ 14000", false],
            ["€  14000", false],
            ["€   14000", false],

            ["€ 14000", false],
            ["$ 14000", false],
            ["£ 14000", false],
            ["EUR 14000", false],
            ["USD 14000", false],
            ["GBP 14000", false],

            ["€ 14 000", false],
            ["€ 14'000", false],
            ["€ 14,000", false],
            ["€ 14.000", false],

            ["€ 14 5000", false],
            ["€ 14'5000", false],
            ["€ 14,5000", false],
            ["€ 14.5000", false],

            ["€ 28000.532", false],
            ["€ 28000,532", false],
            ["€ 28000.53", false],
            ["€ 28000,53", false],
            ["€ 28000.5", false],
            ["€ 28000,5", false],
            ["€ 28000.", false],
            ["€ 28000,", false],
        ];
    }

    /**
     * @dataProvider containsPriceDotProvider
     * @return void
     */
    public function testContainsPriceDot($value, $expected) {
        $this->assertSame($expected, !empty(Utils::containsPriceDot($value)));
    }

    public function containsPriceDotInvertProvider() {
        return [
            ["14000€", false],
            ["14000 €", false],
            ["14000  €", false],
            ["14000   €", false],

            ["14000 €", false],
            ["14000 $", false],
            ["14000 £", false],
            ["14000 EUR", false],
            ["14000 USD", false],
            ["14000 GBP", false],

            ["14 000 €", false],
            ["14'000 €", false],
            ["14,000 €", false],
            ["14.000 €", false],

            ["14 5000 €", false],
            ["14'5000 €", false],
            ["14,5000 €", false],
            ["14.5000 €", false],

            ["28000.532 €", false],
            ["28000,532 €", false],
            ["28000.53 €", false],
            ["28000,53 €", false],
            ["28000.5 €", false],
            ["28000,5 €", false],
            ["28000. €", false],
            ["28000, €", false],

            ["€14000", false],
            ["€ 14000", false],
            ["€  14000", false],
            ["€   14000", false],

            ["€ 14000", false],
            ["$ 14000", false],
            ["£ 14000", false],
            ["EUR 14000", false],
            ["USD 14000", false],
            ["GBP 14000", false],

            ["€ 14 000", false],
            ["€ 14'000", false],
            ["€ 14,000", false],
            ["€ 14.000", false],

            ["€ 14 5000", false],
            ["€ 14'5000", false],
            ["€ 14,5000", false],
            ["€ 14.5000", false],

            ["€ 28000.532", false],
            ["€ 28000,532", false],
            ["€ 28000.53", true],
            ["€ 28000,53", false],
            ["€ 28000.5", true],
            ["€ 28000,5", false],
            ["€ 28000.", false],
            ["€ 28000,", false],
        ];
    }

    /**
     * @dataProvider containsPriceDotInvertProvider
     * @return void
     */
    public function testContainsPriceDotInvert($value, $expected) {
        $this->assertSame($expected, !empty(Utils::containsPriceDotInvert($value)));
    }

    public function containsPriceCommaProvider() {
        return [
            ["14000€", false],
            ["14000 €", false],
            ["14000  €", false],
            ["14000   €", false],

            ["14000 €", false],
            ["14000 $", false],
            ["14000 £", false],
            ["14000 EUR", false],
            ["14000 USD", false],
            ["14000 GBP", false],

            ["14 000 €", false],
            ["14'000 €", false],
            ["14,000 €", false],
            ["14.000 €", false],

            ["14 5000 €", false],
            ["14'5000 €", false],
            ["14,5000 €", false],
            ["14.5000 €", false],

            ["28000.532 €", false],
            ["28000,532 €", false],
            ["28000.53 €", false],
            ["28000,53 €", true],
            ["28000.5 €", false],
            ["28000,5 €", true],
            ["28000. €", false],
            ["28000, €", false],

            ["€14000", false],
            ["€ 14000", false],
            ["€  14000", false],
            ["€   14000", false],

            ["€ 14000", false],
            ["$ 14000", false],
            ["£ 14000", false],
            ["EUR 14000", false],
            ["USD 14000", false],
            ["GBP 14000", false],

            ["€ 14 000", false],
            ["€ 14'000", false],
            ["€ 14,000", false],
            ["€ 14.000", false],

            ["€ 14 5000", false],
            ["€ 14'5000", false],
            ["€ 14,5000", false],
            ["€ 14.5000", false],

            ["€ 28000.532", false],
            ["€ 28000,532", false],
            ["€ 28000.53", false],
            ["€ 28000,53", false],
            ["€ 28000.5", false],
            ["€ 28000,5", false],
            ["€ 28000.", false],
            ["€ 28000,", false],
        ];
    }

    /**
     * @dataProvider containsPriceCommaProvider
     * @return void
     */
    public function testContainsPriceComma($value, $expected) {
        $this->assertSame($expected, !empty(Utils::containsPriceComma($value)));
    }

    public function containsPriceCommaInvertProvider() {
        return [
            ["14000€", false],
            ["14000 €", false],
            ["14000  €", false],
            ["14000   €", false],

            ["14000 €", false],
            ["14000 $", false],
            ["14000 £", false],
            ["14000 EUR", false],
            ["14000 USD", false],
            ["14000 GBP", false],

            ["14 000 €", false],
            ["14'000 €", false],
            ["14,000 €", false],
            ["14.000 €", false],

            ["14 5000 €", false],
            ["14'5000 €", false],
            ["14,5000 €", false],
            ["14.5000 €", false],

            ["28000.532 €", false],
            ["28000,532 €", false],
            ["28000.53 €", false],
            ["28000,53 €", false],
            ["28000.5 €", false],
            ["28000,5 €", false],
            ["28000. €", false],
            ["28000, €", false],

            ["€14000", false],
            ["€ 14000", false],
            ["€  14000", false],
            ["€   14000", false],

            ["€ 14000", false],
            ["$ 14000", false],
            ["£ 14000", false],
            ["EUR 14000", false],
            ["USD 14000", false],
            ["GBP 14000", false],

            ["€ 14 000", false],
            ["€ 14'000", false],
            ["€ 14,000", false],
            ["€ 14.000", false],

            ["€ 14 5000", false],
            ["€ 14'5000", false],
            ["€ 14,5000", false],
            ["€ 14.5000", false],

            ["€ 28000.532", false],
            ["€ 28000,532", false],
            ["€ 28000.53", false],
            ["€ 28000,53", true],
            ["€ 28000.5", false],
            ["€ 28000,5", true],
            ["€ 28000.", false],
            ["€ 28000,", false],
        ];
    }

    /**
     * @dataProvider containsPriceCommaInvertProvider
     * @return void
     */
    public function testContainsPriceCommaInvert($value, $expected) {
        $this->assertSame($expected, !empty(Utils::containsPriceCommaInvert($value)));
    }
}
