<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $files = Storage::disk('seeder')->files();

        foreach($files as $file) {
            if (Storage::disk('seeder')->exists($file)) {
                $content = Storage::disk('seeder')->get($file);

                DB::table('invoices')->insert([
                    'name' => $file,
                    'content' => $content,
                    'created_at' => Carbon::now(),
                ]);
            }
        }
        
    }
}
