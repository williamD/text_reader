<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="bg-white">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>Laravel</title>
    </head>
    <body class="antialiased">
        
        @include('partials.banner')
        <div class="container mx-auto pb-8 space-y-6">
        @if($results)
            @foreach($results as $result)
            <div>
                <h2 class="mx-auto mt-8 text-lg leading-6 font-medium text-gray-900">
                    {{$result['filename']}}
                </h2>
                @if($result)
                    @if(array_key_exists(0, $result))
                        <div class="space-y-4">
                            @foreach($result as $datatable)
                                @if(is_array($datatable))
                                    <div class="mt-2 flex flex-col">
                                        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                                            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                                                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                                    <table class="min-w-full divide-y divide-gray-200">
                                                        <thead class="bg-gray-50">
                                                            <tr>
                                                                @foreach($datatable['header'] as $headerItem)
                                                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                    {{$headerItem['value']}}
                                                                </th>
                                                                @endforeach
                                                            </tr>
                                                        </thead>
                                                        <tbody class="bg-white divide-y divide-gray-200">
                                                            @foreach($datatable['lines'] as $line)
                                                            <tr>
                                                                @foreach($line['columns'] as $lineItem)
                                                                <td class="px-6 py-4 text-sm text-gray-500 whitespace-nowrap">
                                                                    @if(is_array($lineItem))
                                                                        @foreach($lineItem as $lineItemValue)
                                                                            <div>{{$lineItemValue}}</div>
                                                                        @endforeach
                                                                    @else
                                                                        {{$lineItem}}
                                                                    @endif
                                                                </td>
                                                                @endforeach
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @else
                        <div class="alert mt-2 rounded-md p-4">
                            <div class="flex">
                                <div class="flex-1 md:flex md:justify-center">
                                    <p class="alert-text text-sm">
                                        Aucun produit récupéré lors de l'analyse de la facture.
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
            </div>
            @endforeach
        @endif
    </div>
    <script>
        const array = ['gray', 'red', 'orange', 'amber', 'yellow', 'lime', 'green', 'emerald', 'teal', 'cyan', 'lightBlue', 'blue', 'indigo', 'violet', 'purple', 'fuchsia', 'pink', 'rose']
        
        const banner = document.querySelectorAll('.banner')
        const bannerLabel = document.querySelectorAll('.banner-label')
        const alert = document.querySelectorAll('.alert')
        const alertText = document.querySelectorAll('.alert-text')

        const randomColor = array[~~(Math.random() * array.length)]

        banner.forEach(el => el.classList.add(`bg-${randomColor}-600`))
        bannerLabel.forEach(el => el.classList.add(`text-${randomColor}-100`))
        alert.forEach(el => el.classList.add(`bg-${randomColor}-50`))
        alertText.forEach(el => el.classList.add(`text-${randomColor}-700`))
    </script>
    </body>
</html>
