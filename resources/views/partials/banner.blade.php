<div class="banner sticky top-0 w-full h-auto">
  <div class="container mx-auto py-3">
    <div class="flex items-center justify-between flex-wrap">
      <div class="w-0 flex-1 flex items-center">
        <p class="font-medium text-white truncate">
          <span class="banner-label inline-block mr-3">Execution time</span><span class="inline-block">{{$bannerContent}}</span>
        </p>
      </div>
    </div>
  </div>
</div>

